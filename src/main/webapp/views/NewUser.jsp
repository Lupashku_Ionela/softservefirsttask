<%@ page import="model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Registration</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="js/jquery.timepicker.min.css">
    <script src="js/jquery.timepicker.min.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/inputStyle.css">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Candal">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<div>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container bg-color-my">
            <div class="col-md-12">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"
                            aria-controls="myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<c:url value = "/"/>"><img src="img/logo.png" class="img-responsive-my"
                                                          style="width: 90px; margin-top: -16px;"></a>
                </div>
                <div class="collapse navbar-collapse navbar-right" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class=""><a href="<c:url value = "/"/>">Home</a></li>
                        <li class="active"><a href="<c:url value = "/registration"/>">Registration</a></li>
                        <li class=""><a href="<c:url value = "/signIn"/>">Sign in</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</div>
<div class="title">
    <b>Registration</b>
</div>
<form action="${pageContext.request.contextPath}/registration" method="POST" id="registration" class="registration">
    <div class="group">
        <label for="first_name" class="label2">
            <input type="text" name="first_name" id="first_name" value="${firstName}" pattern="^[A-Z][a-z]{2,14}$" required>
            <span class="bar"></span>
            <label class="myLabel">First name</label>
        </label>
    </div>
    <div class="group">
        <label for="last_name" class="label2">
            <input type="text" name="last_name" id="last_name" value="${lastName}" pattern="^[A-Z][a-z]{2,14}$" required>
            <span class="bar"></span>
            <label class="myLabel">Last name</label>
        </label>
    </div>
    <div class="group" id="dateBirth" >
        <label for="dateB" class="label2">
            <input type="date" name="dateB" id="dateB" required>
            <span class="bar"></span>
            <label class="myLabel">Date of birth</label>
            <ul class="input-requirements" style="display: none">
                <li>Must be before current date</li>
            </ul>
        </label>
    </div>
    <div class="group" id="phonee" >
        <label for="phone" class="label2" >
            <input type="text" name="phone" id="phone" value="${phone}" pattern="^\+380\d{3}\d{2}\d{2}\d{2}$" required>
            <span class="bar"></span>
            <label class="myLabel">Phone</label>
        </label>
    </div>
    <div class="errorMessage group">
        <c:forEach items="${registerError}" var="error">
            <span class="errorMessage">${error}</span>
        </c:forEach>
    </div>
    <div class="group">
        <label for="login" class="label2">
            <input type="text" name="login" id="login" value="${login}" pattern="^[a-zA-Z][a-zA-Z0-9\\-\\_\\.]{1,18}[a-zA-Z0-9]$" required>
            <span class="bar"></span>
            <label class="myLabel">Login</label>
        </label>
    </div>
    <div class="group">
        <label for="password" class="label2">
            <input type="password" name="password" id="password"
                   pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$" required>
            <span class="bar"></span>
            <label class="myLabel">Password</label>
        </label>
    </div>
    <div class="group">
        <label for="confirm_password" class="label2">
            <input type="password" name="confirm_password" id="confirm_password"
                   pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$" required>
            <span class="bar"></span>
            <label class="myLabel">Confirm password</label>
        </label>
    </div>
    <div align="center"><input type="submit" name="buttonAdd" value="Register"></div>
</form>

<script src="js/jquery.easing.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>

<script language="JavaScript" type="text/JavaScript">

    $(window).load(function(){
        document.getElementById("dateB").value = "${dateB}";
        var today = new Date();
        var min = new Date();
        var dd = today.getDate() - 1;
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        min = (yyyy - 120) + '-' + mm + '-' + dd;
        today = yyyy + '-' + mm + '-' + dd;
        document.getElementById("dateB").setAttribute("max", today);
        document.getElementById("dateB").setAttribute("min", min);
    });
</script>

</body>
</html>


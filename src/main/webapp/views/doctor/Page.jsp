<%@ page import="model.User" %>
<%@ page import="org.joda.time.format.DateTimeFormat" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Profile</title>
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Candal">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="css/card.css">

</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid  bg-color-my ">
        <div class="col-md-12">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<c:url value = "/"/>"><img src="img/logo.png"
                                                                         class="img-responsive-my"
                                                                         style="width: 90px; margin-top: -16px;"></a>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
                <ul class="nav navbar-nav">
                    <% User user = (User) session.getAttribute("currUser");
                        if (user != null) {
                            if (user.getRole().toString().equals(String.valueOf("DOCTOR"))) {%>
                    <li><a href="<c:url value = "/doctorsAppointments"/>">Appointments</a></li>
                    <li class="dropdown">
                        <a href="<c:url value = "/doctor"/>" class="dropdown-toggle"
                           data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">${doctor.firstName} ${doctor.lastName}<span class="caret"></span></a>
                        <ul class="myMenu dropdown-menu">
                            <li><a href="<c:url value = "/doctor"/>" class="myLi">Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<c:url value="/logout"/>" class="myLi">Log out</a></li>
                        </ul>
                    </li>
                    <%
                            }
                        }
                    %>
                </ul>
            </div>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row card cardWidth col-centered">
        <div class="card-profile">
            <div class="col-lg-3 col-md-2 col-sm-2">
                <div class="card-avatar">
                    <img id="logo" class="svg img" src="img/user-md-solid.svg">
                </div>
            </div>
            <div class="col-lg-7 col-md-8 col-sm-8">
                <div class="boxHidden">
                    <span class="errorMessage" style="color: red">${error[0]}</span>
                    <span class="errorMessage" style="color: red">${errors}</span>
                </div>
                <h4 class="card-title">${doctor.firstName} ${doctor.lastName}</h4>
                <hr class="hrProfile">
                <div class="row">
                    <div class="col-md-6">
                        <label><i class="fas fa-star-half-alt"> Login </i></label>
                    </div>
                    <div class="col-md-6">
                        <p class="pp">${doctor.login}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label><i class="fas fa-clock"> Work time </i></label>
                    </div>
                    <div class="col-md-6">
                        <p class="pp">${doctor.startTime.toString(DateTimeFormat.forPattern("HH:mm"))}
                            - ${doctor.endTime.toString(DateTimeFormat.forPattern("HH:mm"))}</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <a href="" class="action-button modalUpd" data-toggle="modal" data-target="#modalUpdateForm">Update</a>
                <a href="" class="action-button " data-toggle="modal" data-target="#modalChangeForm">Password</a>
                <a href="" class="action-button " data-toggle="modal" data-target="#modalDeleteForm">Delete</a>
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="modalUpdateForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Update profile</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="${pageContext.request.contextPath}/updateDoctor">
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <div class="group">
                            <input type="text" class="putName" name="firstName" id="fName" value="${doctor.firstName}"
                                   pattern="^[A-Z][a-z]{2,14}$" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label data-error="wrong" data-success="right" for="fName">First name</label>
                        </div>
                    </div>
                    <div class="md-form mb-5">
                        <div class="group">
                            <input type="text" class="putName" name="lastName" id="lName" value="${doctor.lastName}"
                                   pattern="^[A-Z][a-z]{2,14}$" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label data-error="wrong" data-success="right" for="lName">Last name</label>
                        </div>
                    </div>
                    <div class="md-form mb-5">
                        <div class="group">
                            <input type="text" class="putName" name="login" id="login" value="${doctor.login}"
                                   pattern="^[a-zA-Z][a-zA-Z0-9\\-\\_\\.]{1,18}[a-zA-Z0-9]$" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label data-error="wrong" data-success="right" for="login">Login</label>
                        </div>
                    </div>
                    <div class="md-form mb-5">
                        <input type="hidden" name="start_time_hours" id="start_time_hours" value="9">
                        <input type="hidden" name="start_time_minutes" id="start_time_minutes" value="0">
                        <input type="hidden" name="end_time_hours" id="end_time_hours" value="15">
                        <input type="hidden" name="end_time_minutes" id="end_time_minutes" value="0">
                        <div id="time-range">
                            <p style="color: #2b2e38; text-align: center; font-size: 18px">Working hours: <span class="slider-time">9:00</span> - <span
                                    class="slider-time2">15:00</span>

                            </p>
                            <div class="sliders_step1">
                                <div id="slider-range"></div>
                            </div>
                        </div>
                        <span class="highlight"></span>
                        <span class="bar"></span>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <input type="submit" class="action-button" value="Update" style="width: 30%">
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalChangeForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Change password</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="${pageContext.request.contextPath}/changePassDoctor">
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <div class="group">
                            <input type="password" class="putName" id="currPass" name="currentPass"
                                   pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label data-error="wrong" data-success="right" for="currPass">Current password</label>
                        </div>
                    </div>
                    <div class="md-form mb-5">
                        <div class="group">
                            <input type="password" class="putName" name="newPass" id="newPass"
                                   pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label data-error="wrong" data-success="right" for="newPass">New password</label>
                        </div>
                    </div>
                    <div class="md-form mb-5">
                        <div class="group">
                            <input type="password" class="putName" name="newConfPass" id="newConfPass"
                                   pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label data-error="wrong" data-success="right" for="newConfPass">Confirm new
                                password</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <input type="submit" class="action-button" value="Change" style="width: 30%">
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalDeleteForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Delete profile</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <label>Are you sure you want to delete your account?</label>
            </div>
            <form method="post" action="${pageContext.request.contextPath}/deleteDoctor">
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <div class="group">
                            <input type="password" class="putName" name="currentPass" id="currentPass"
                                   pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label data-error="wrong" data-success="right" for="currentPass">Current password</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <input type="submit" class="action-button" value="VES" style="width: 45%">
                    <input type="button" class="action-button" data-dismiss="modal" style="width: 45%" value="NO">
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    setTimeout(function () {
        $('.boxHidden').fadeOut('fast')
    }, 3000);

    jQuery('img.svg').each(function () {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function (data) {
            var $svg = jQuery(data).find('svg');
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }
            $svg = $svg.removeAttr('xmlns:a');
            $img.replaceWith($svg);

        }, 'xml');

    });
    $("#slider-range").slider({
        range: true,
        min: 0,
        max: 1440,
        step: 10,
        values: [540, 1020],
        slide: function (e, ui) {
            var hours1 = Math.floor(ui.values[0] / 60);
            var minutes1 = ui.values[0] - (hours1 * 60);

            if (hours1.length == 1) hours1 = '0' + hours1;
            if (minutes1.length == 1) minutes1 = '0' + minutes1;
            if (minutes1 == 0) minutes1 = '00';
                if (hours1 == 24) {
                    hours1 = 23;
                    minutes1 = "58";
                }

            if (hours1 == 0) {
                hours1 = 0;
                minutes1 = minutes1;
            }


            $('.slider-time').html(hours1 + ':' + minutes1);
            document.getElementById("start_time_hours").value = hours1;
            document.getElementById("start_time_minutes").value = minutes1;

            var hours2 = Math.floor(ui.values[1] / 60);
            var minutes2 = ui.values[1] - (hours2 * 60);

            if (hours2.length == 1) hours2 = '0' + hours2;
            if (minutes2.length == 1) minutes2 = '0' + minutes2;
            if (minutes2 == 0) minutes2 = '00';
            if (hours2 >= 23) {
                if (hours2 == 24) {
                    hours2 = 23;
                    minutes2 = "59";
                }
            } else {
                hours2 = hours2;
                minutes2 = minutes2;
            }

            $('.slider-time2').html(hours2 + ':' + minutes2);
            document.getElementById("end_time_hours").value = hours2;
            document.getElementById("end_time_minutes").value = minutes2;
        }
    });


</script>
</body>

</html>


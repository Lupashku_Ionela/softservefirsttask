<%@ page import="model.User" %>
<%@ page import="model.Patient" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Patients</title>
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Candal">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="css/card.css">


</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid  bg-color-my ">
        <div class="col-md-12">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<c:url value = "/"/>"><img src="img/logo.png"
                                                                         class="img-responsive-my"
                                                                         style="width: 90px; margin-top: -16px;"></a>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
                <ul class="nav navbar-nav">
                    <% User user = (User) session.getAttribute("currUser");
                        if (user != null){
                            if(user.getRole().toString().equals(String.valueOf("ADMIN"))) {%>
                    <li><a href="<c:url value = "/patients"/>">Patients</a></li>
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle"
                           data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">Doctors<span class="caret"></span></a>
                        <ul class="dropdown-menu myMenu">
                            <li><a href="<c:url value = "/doctors"/>" class="myLi">All Doctors</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<c:url value = "/newDoctor"/>" class="myLi">Add Doctor</a></li>
                        </ul>
                    </li>
                    <li><a href="<c:url value="/logout"/>" class="myLi">Log out</a></li>
                    <%}%>

                    <%}%>
                </ul>
            </div>
        </div>
    </div>
</nav>

<div class="container">
    <div class="row card cardWidth col-centered" >
        <div class="card-profile">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h4 class="card-title">Active Patients </h4>
                <div class="boxHidden">
                    <span class="errorMessage" style="color: red; font-size: 20px;">${errors[0]}</span>
                    <span style="color: #3c763d; font-size: 20px;">${success[0]}</span>
                </div>
                <% if (((List<Patient>) request.getAttribute("activePatients")).size()>0){ %>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">Firs name</th>
                        <th scope="col">Last name</th>
                        <th scope="col">Login</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="patient" type="model.Patient"--%>
                    <c:forEach items="${activePatients}" var="patient">
                        <tr>
                            <th scope="row">${patient.firstName}</th>
                            <th scope="row">${patient.lastName}</th>
                            <th scope="row">${patient.login}</th>
                            <td>
                                <form method="post" action="${pageContext.request.contextPath}/changeStatus">
                                    <input type="hidden" value="${patient.id}" name="idUser">
                                    <input type="hidden" value="1" name="status">
                                    <input type="submit" class="action-button" style="width: 50%" value="Block">
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <% }else{ %>
                <br>
                <label >There are no active patients now.</label>
                <% }%>
            </div>
        </div>
    </div>
</div>
</div>
<div class="container">
    <div class="row card cardWidth col-centered" style="margin-top: 0">
        <div class="card-profile">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h4 class="card-title">Disabled Patients </h4>
                <div class="boxHidden">
                    <span class="errorMessage" style="color: red; font-size: 20px;">${errors[0]}</span>
                    <span style="color: #3c763d; font-size: 20px;">${success[0]}</span>
                </div>
                <% if (((List<Patient>) request.getAttribute("disabledPatients")).size()>0){ %>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">Firs name</th>
                        <th scope="col">Last name</th>
                        <th scope="col">Login</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="patient" type="model.Patient"--%>
                    <c:forEach items="${disabledPatients}" var="patient">
                        <tr>
                            <th scope="row">${patient.firstName}</th>
                            <th scope="row">${patient.lastName}</th>
                            <th scope="row">${patient.login}</th>
                            <td>
                                <form method="post" action="${pageContext.request.contextPath}/changeStatus">
                                    <input type="hidden" value="${patient.id}" name="idUser">
                                    <input type="hidden" value="0" name="status">
                                    <input type="submit" class="action-button" style="width: 50%" value="Unblock">
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <% }else{ %>
                <br>
                <label >There are no disable patients now.</label>
                <% }%>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    function validate() {
        var pass2 = $("#confirm_password");
        if ($("#password").value !== pass2.value){
            pass2.setCustomValidity("Passwords Don't Match!");
        }else {
            pass2.setCustomValidity('');
        }

        $.get(window.location.origin+"/checkLogin?login="+$('#login').value).done(function (resp) {
            console.log(resp);
            if (resp === "false"){
                $("#login").setCustomValidity("User with this login is already exist.");
            }else{
                $("#login").setCustomValidity('');
            }
        });
    }
</script>
</body>

</html>

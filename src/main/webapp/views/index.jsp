<%@ page import="model.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Candal">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="css/card.css">

</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<!--banner-->
<section id="banner" class="banner">
    <div class="bg-color">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="col-md-12">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<c:url value = "/"/>"><img src="img/logo.png"
                                                                                 class="img-responsive-my"
                                                                                 style="width: 90px; margin-top: -16px;"></a>
                    </div>
                    <div class="collapse navbar-collapse navbar-right" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <% if (session.getAttribute("currUser") == null) {%>
                            <li class=""><a href="<c:url value = "/registration"/>">Registration</a></li>
                            <li class=""><a href="<c:url value = "/signIn"/>">Sign in</a></li>
                            <%}%>
                            <% User user = (User) session.getAttribute("currUser");
                                if (user != null) {
                                    if (user.getRole().toString().equals(String.valueOf("PATIENT"))) {%>
                            <li class="dropdown" style="    min-width: 180px;">
                                <a href="" class="dropdown-toggle"
                                   data-toggle="dropdown" role="button" aria-haspopup="true"
                                   aria-expanded="false">Appointments<span class="caret"></span></a>
                                <ul class="myMenu dropdown-menu">
                                    <li><a href="<c:url value = "/appointments"/>" class="myLi">My Appointments</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="<c:url value="/allDoctors"/>" class="myLi">Make an Appointment</a></li>
                                </ul>
                            </li>
                            <li class="dropdown" >
                                <a href="<c:url value = "/patient"/>" class="dropdown-toggle"
                                   data-toggle="dropdown" role="button" aria-haspopup="true"
                                   aria-expanded="false">${currUser.firstName} ${currUser.lastName}<span
                                        class="caret"></span></a>
                                <ul class="myMenu dropdown-menu">
                                    <li><a href="<c:url value = "/patient"/>" class="myLi">Profile</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="<c:url value="/logout"/>" class="myLi">Log out</a></li>
                                </ul>
                            </li>
                            <%
                                }
                                if (user.getRole().toString().equals(String.valueOf("DOCTOR"))) {
                            %>
                            <li><a href="<c:url value = "/doctorsAppointments"/>">Appointments</a></li>
                            <li class="dropdown">
                                <a href="<c:url value = "/doctor"/>" class="dropdown-toggle"
                                   data-toggle="dropdown" role="button" aria-haspopup="true"
                                   aria-expanded="false">${currUser.firstName} ${currUser.lastName}<span class="caret"></span></a>
                                <ul class="myMenu dropdown-menu">
                                    <li><a href="<c:url value = "/doctor"/>" class="myLi">Profile</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="<c:url value="/logout"/>" class="myLi">Log out</a></li>
                                </ul>
                            </li>
                            <%
                                }
                                if (user.getRole().toString().equals(String.valueOf("ADMIN"))) {
                            %>
                            <li><a href="<c:url value = "/patients"/>">Patients</a></li>
                            <li class="dropdown">
                                <a href="" class="dropdown-toggle"
                                   data-toggle="dropdown" role="button" aria-haspopup="true"
                                   aria-expanded="false">Doctors<span class="caret"></span></a>
                                <ul class="dropdown-menu myMenu">
                                    <li><a href="<c:url value = "/doctors"/>" class="myLi">All Doctors</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="<c:url value = "/newDoctor"/>" class="myLi">Add Doctor</a></li>
                                </ul>
                            </li>
                            <li><a href="<c:url value="/logout"/>" class="myLi">Log out</a></li>
                            <%}%>

                            <%}%>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="banner-info">

                    <div class="banner-text text-center" style="margin-top: 200px">
                        <h1 class="white">Healthcare at your desk!!</h1>
                        <%if (user != null) {
                            if (user.getRole().toString().equals(String.valueOf("PATIENT"))) {%>
                        <a href="<c:url value="${pageContext.request.contextPath}/allDoctors"/>" class="btn btn-appoint">Make an Appointment.</a>
                        <%}}%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/ banner-->


</body>

</html>
<%@ page import="model.User" %>
<%@ page import="org.joda.time.format.DateTimeFormat" %>
<%@ page import="model.Doctor" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>All doctors</title>
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Candal">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="css/card.css">

</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid  bg-color-my ">
        <div class="col-md-12">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<c:url value = "/"/>"><img src="img/logo.png"
                                                                         class="img-responsive-my"
                                                                         style="width: 90px; margin-top: -16px;"></a>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
                <ul class="nav navbar-nav">
                    <% User user = (User) session.getAttribute("currUser");
                        if (user != null) {
                            if (user.getRole().toString().equals(String.valueOf("PATIENT"))) {%>
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle"
                           data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">Appointments<span class="caret"></span></a>
                        <ul class="myMenu dropdown-menu" style="    min-width: 180px;">
                            <li><a href="<c:url value = "/appointments"/>" class="myLi">My Appointments</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<c:url value="/allDoctors"/>" class="myLi">Make an Appointment</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="<c:url value = "/patient"/>" class="dropdown-toggle"
                           data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">${patient.firstName} ${patient.lastName}<span class="caret"></span></a>
                        <ul class="myMenu dropdown-menu">
                            <li><a href="<c:url value = "/patient"/>" class="myLi">Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<c:url value="/logout"/>" class="myLi">Log out</a></li>
                        </ul>
                    </li>
                    <%
                            }
                        }
                    %>
                </ul>
            </div>
        </div>
    </div>
</nav>
<div class="container" >
    <div class="row" style=" margin-top: 150px;">
        <h1 class="card-title" style="text-align: center; font-size: xx-large; border-bottom: #2b2e38">DOCTORS</h1>
        <%--@elvariable id="doctor" type="model.Doctor"--%>
        <% if (((List<Doctor>) request.getAttribute("doctors")).size()>0){ %>
        <c:forEach items="${doctors}" var="doctor">
            <div class="col-lg-4 col-md-6 col-sm-8">
                <div class="card1">
                    <div class="card-body">
                        <h5 class="card-title">${doctor.firstName} ${doctor.lastName}</h5>
                        <p class="card-text">Work time:</p>
                        <p class="card-text">${doctor.startTime.toString(DateTimeFormat.forPattern("HH:mm"))}
                            - ${doctor.endTime.toString(DateTimeFormat.forPattern("HH:mm"))}</p>
                        <form method="post" action="${pageContext.request.contextPath}/addRecordDoctor">
                            <input type="hidden" value="${doctor.id}" name="idDoctor">
                            <input type="submit" class="action-button" style="width: 50%" value="Make an Appointment">
                        </form>
                    </div>
                </div>
            </div>
        </c:forEach>
            <% }else{ %>
            <br>
            <label >There are no doctors now.</label>
            <% }%>
    </div>
</div>

<script>



</script>
</body>

</html>



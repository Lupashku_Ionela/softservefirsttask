<%@ page import="model.User" %>
<%@ page import="org.joda.time.format.DateTimeFormat" %>
<%@ page import="model.Record" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>My appointments</title>
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Candal">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="css/card.css">

</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid  bg-color-my ">
        <div class="col-md-12">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<c:url value = "/"/>"><img src="img/logo.png"
                                                                         class="img-responsive-my"
                                                                         style="width: 90px; margin-top: -16px;"></a>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
                <ul class="nav navbar-nav">
                    <% User user = (User) session.getAttribute("currUser");
                        if (user != null) {
                            if (user.getRole().toString().equals(String.valueOf("PATIENT"))) {%>
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle"
                           data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">Appointments<span class="caret"></span></a>
                        <ul class="myMenu dropdown-menu" style="    min-width: 180px;">
                            <li><a href="<c:url value = "/appointments"/>" class="myLi">My Appointments</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<c:url value="/allDoctors"/>" class="myLi">Make an Appointment</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="<c:url value = "/patient"/>" class="dropdown-toggle"
                           data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">${patient.firstName} ${patient.lastName}<span class="caret"></span></a>
                        <ul class="myMenu dropdown-menu">
                            <li><a href="<c:url value = "/patient"/>" class="myLi">Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<c:url value="/logout"/>" class="myLi">Log out</a></li>
                        </ul>
                    </li>
                    <%
                            }
                        }
                    %>
                </ul>
            </div>
        </div>
    </div>
</nav>

<div class="container">
    <div class="row card cardWidth col-centered">
        <div class="card-profile">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <h4 class="card-title">${patient.firstName} ${patient.lastName}</h4>
                <div class="boxHidden">
                    <span class="errorMessage" style="color: red; font-size: 20px;">${errors[0]}</span>
                    <span style="color: #3c763d; font-size: 20px;">${success[0]}</span>
                </div>
                <% if (((List<Record>) request.getAttribute("records")).size()>0){ %>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">Time</th>
                        <th scope="col">Doctor</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="record" type="model.Record"--%>
                    <c:forEach items="${records}" var="record">
                        <tr>
                            <th scope="row">${record.start.toString(DateTimeFormat.forPattern("HH:mm"))}
                                - ${record.end.toString(DateTimeFormat.forPattern("HH:mm"))}</th>
                            <td>${record.doctor.firstName} ${record.doctor.lastName}</td>
                            <td>
                                <form method="post" action="${pageContext.request.contextPath}/appointments">
                                    <input type="hidden" value="${record.id}" name="idRecord">
                                    <input type="submit" class="action-button" style="width: 50%" value="REVOKE">
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <% }else{ %>
                <br>
                <label >You don't have visits yet.</label>
                <% }%>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">

    setTimeout(function () {
        $('.boxHidden').fadeOut('fast')
    }, 3000);

    jQuery('img.svg').each(function () {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function (data) {
            var $svg = jQuery(data).find('svg');
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }
            $svg = $svg.removeAttr('xmlns:a');
            $img.replaceWith($svg);

        }, 'xml');

    });
</script>
</body>

</html>




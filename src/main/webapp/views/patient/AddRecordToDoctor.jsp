<%@ page import="model.User" %>
<%@ page import="org.joda.time.format.DateTimeFormat" %>
<%@ page import="model.Record" %>
<%@ page import="java.util.List" %>
<%@ page import="org.joda.time.LocalTime" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Add record</title>
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Candal">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="js/jquery.timepicker.min.css">

    <script src="js/jquery.timepicker.min.js"></script>
    <link rel="stylesheet" href="css/card.css">

</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid  bg-color-my ">
        <div class="col-md-12">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<c:url value = "/"/>"><img src="img/logo.png"
                                                                         class="img-responsive-my"
                                                                         style="width: 90px; margin-top: -16px;"></a>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
                <ul class="nav navbar-nav">
                    <% User user = (User) session.getAttribute("currUser");
                        if (user != null) {
                            if (user.getRole().toString().equals(String.valueOf("PATIENT"))) {%>
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle"
                           data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">Appointments<span class="caret"></span></a>
                        <ul class="myMenu dropdown-menu" style="    min-width: 180px;">
                            <li><a href="<c:url value = "/appointments"/>" class="myLi">My Appointments</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<c:url value="/allDoctors"/>" class="myLi">Make an Appointment</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="<c:url value = "/patient"/>" class="dropdown-toggle"
                           data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">${patient.firstName} ${patient.lastName}<span class="caret"></span></a>
                        <ul class="myMenu dropdown-menu">
                            <li><a href="<c:url value = "/patient"/>" class="myLi">Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<c:url value="/logout"/>" class="myLi">Log out</a></li>
                        </ul>
                    </li>
                    <%
                            }
                        }
                    %>
                </ul>
            </div>
        </div>
    </div>
</nav>
<div class="container" style="margin-top: 150px">
    <div class="row">
        <div class="col-lg-5 col-md-3 col-sm-3">
            <div class="card1">
                <div class="card-body">
                    <h5 class="card-title">Free work times of ${doctor.firstName} ${doctor.lastName}</h5>
                    <p class="card-text" style="font-size: 20px">Work
                        time: ${doctor.startTime.toString(DateTimeFormat.forPattern("HH:mm"))}
                        - ${doctor.endTime.toString(DateTimeFormat.forPattern("HH:mm"))}</p>
                    <%--@elvariable id="range" type="model.Record"--%>
                    <% if (((List<Record>) request.getAttribute("ranges")).size()>0){ %>
                    <c:forEach items="${ranges}" var="range">
                        <p class="card-text">${range.start.toString(DateTimeFormat.forPattern("HH:mm"))}
                            - ${range.end.toString(DateTimeFormat.forPattern("HH:mm"))}</p>
                    </c:forEach>
                    <% }else{ %>
                    <br>
                    <label >Doctor is busy all work day</label>
                    <% }%>
                </div>
            </div>
        </div>
        <div class="col-lg-7 col-md-9 col-sm-9 ">
            <div class="card1" >
                <div class="card-body col-centered">
                    <div class="row">
                        <div class="col-lg-12 col-md-10 col-sm-12 col-centered">
                            <h5 class="card-title">Book an appointment</h5>
                            <div class="boxHidden">
                                <span class="errorMessage" style="color: red; font-size: 20px;">${errors[0]}</span>
                                <span style="color: #3c763d; font-size: 20px;">${success[0]}</span>
                            </div>
                            <form method="post" action="${pageContext.request.contextPath}/record">
                                <div class="modal-body mx-3">
                                    <input type="hidden" name="idDoctor" id="idDoctor" value="${doctor.id}">
                                    <div class="md-form mb-5">
                                        <div class="group" style="margin-left: 25%">
                                            <input type="text" name="start_time" id="start_time" autocomplete="off"
                                                   value="${time.toString(DateTimeFormat.forPattern("HH:mm"))}"
                                                   required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label data-error="wrong" data-success="right" for="start_time">Time</label>
                                        </div>
                                    </div>
                                    <div class="md-form mb-5">
                                        <div class="group" style="margin-left: 25%">
                                            <input type="number" name="duration" id="duration" style="width: 50%;"
                                                   min="1" value="${duration}" required>
                                            <span class="highlight"></span>
                                            <span class="bar"></span>
                                            <label data-error="wrong" data-success="right"
                                                   for="duration">Duration</label>
                                        </div>
                                    </div>
                                    <input type="submit" name="submit_change" class="action-button" value="Confirm"
                                           style="width: 50%; font-size: 15px">
                                </div>
                            </form>
                            <% if (request.getAttribute("errors") != null) {%>
                            <a href="" class="action-button" data-toggle="modal" data-target="#modalTimesForm"
                               style="width: 25%; font-size: 15px">Free times for this duration</a>
                            <a href="" class="action-button" data-toggle="modal" data-target="#modalNearestTimeForm"
                               style="width: 25%; font-size: 15px">Nearest time for this duration</a>
                            <%}%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalTimesForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-doc">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Free times for appointment ${duration} min</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <%--@elvariable id="t" type="org.joda.time.LocalTime"--%>
            <% if (request.getAttribute("times") !=null){
                if (((List<LocalTime>) request.getAttribute("times")).size()>0){ %>
            <c:forEach items="${times}" var="t">
                <p class="card-text">${t.toString(DateTimeFormat.forPattern("HH:mm"))}</p>
            </c:forEach>
            <% }else{ %>
            <br>
            <label >The doctor has no free time</label>
            <% }%>
            <% }%>
            <input type="button" class="action-button" data-dismiss="modal" style="width: 45%" value="OK">
        </div>
    </div>
</div>

<div class="modal fade" id="modalNearestTimeForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-doc">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Nearest time for appointment ${duration} min</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <% if (request.getAttribute("nearestTime") !=null){ %>
            <p class="card-text">${nearestTime.toString(DateTimeFormat.forPattern("HH:mm"))}</p>
            <% }else{ %>
            <br>
            <label >The doctor has no free time</label>
            <% }%>
            <input type="button" class="action-button" data-dismiss="modal" style="width: 45%" value="OK">
        </div>
    </div>
</div>


<script>
    setTimeout(function () {
        $('.boxHidden').fadeOut('fast')
    }, 3000);

    $(window).load(function () {
        $("#start_time").timepicker('option', 'minTime', new Date());
    });
    $("#start_time").timepicker({
        timeFormat: 'HH:mm',
        interval: 15,
        scrollbar: true
    });
</script>
</body>

</html>



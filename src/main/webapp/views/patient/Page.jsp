<%@ page import="model.User" %>
<%@ page import="org.joda.time.format.DateTimeFormat" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Profile</title>
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Candal">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="css/card.css">

</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid  bg-color-my ">
        <div class="col-md-12">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<c:url value = "/"/>"><img src="img/logo.png"
                                                                         class="img-responsive-my"
                                                                         style="width: 90px; margin-top: -16px;"></a>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
                <ul class="nav navbar-nav">
                    <% User user = (User) session.getAttribute("currUser");
                        if (user != null) {
                            if (user.getRole().toString().equals(String.valueOf("PATIENT"))) {%>
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle"
                           data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">Appointments<span class="caret"></span></a>
                        <ul class="myMenu dropdown-menu" style="    min-width: 180px;">
                            <li><a href="<c:url value = "/appointments"/>" class="myLi">My Appointments</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<c:url value="/allDoctors"/>" class="myLi">Make an Appointment</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="<c:url value = "/patient"/>" class="dropdown-toggle"
                           data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">${patient.firstName} ${patient.lastName}<span class="caret"></span></a>
                        <ul class="myMenu dropdown-menu">
                            <li><a href="<c:url value = "/patient"/>" class="myLi">Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<c:url value="/logout"/>" class="myLi">Log out</a></li>
                        </ul>
                    </li>
                    <%
                            }
                        }
                    %>
                </ul>
            </div>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row card cardWidth col-centered">
        <div class="card-profile">
            <div class="col-lg-3 col-md-2 col-sm-2">
                <div class="card-avatar">
                    <img id="logo" class="svg img" src="img/user-injured-solid.svg">
                </div>
            </div>
            <div class="col-lg-7 col-md-8 col-sm-8">
                <div class="boxHidden">
                    <span class="errorMessage" style="color: red">${error[0]}</span>
                    <span class="errorMessage" style="color: red">${errors}</span>
                </div>
                <h4 class="card-title">${patient.firstName} ${patient.lastName}</h4>
                <hr class="hrProfile">
                <div class="row">
                    <div class="col-md-6">
                        <label><i class="fas fa-star-half-alt"> Login </i></label>
                    </div>
                    <div class="col-md-6">
                        <p class="pp">${patient.login}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label><i class="fas fa-calendar-alt"> Date of birth </i></label>
                    </div>
                    <div class="col-md-6">
                        <p class="pp">${patient.dateOfBirth}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label><i class="fas fa-phone"> Phone </i></label>
                    </div>
                    <div class="col-md-6">
                        <p class="pp">${patient.phone}</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
                <a href="" class="action-button modalUpd" data-toggle="modal" data-target="#modalUpdateForm">Update</a>
                <a href="" class="action-button " data-toggle="modal" data-target="#modalChangeForm">Password</a>
                <a href="" class="action-button " data-toggle="modal" data-target="#modalDeleteForm">Delete</a>
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="modalUpdateForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Update profile</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="${pageContext.request.contextPath}/updatePatient">
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <div class="group">
                            <input type="text" class="putName" name="firstName" id="fName" value="${patient.firstName}"
                                   pattern="^[A-Z][a-z]{2,14}$" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label data-error="wrong" data-success="right" for="fName">First name</label>
                        </div>
                    </div>
                    <div class="md-form mb-5">
                        <div class="group">
                            <input type="text" class="putName" name="lastName" id="lName" value="${patient.lastName}"
                                   pattern="^[A-Z][a-z]{2,14}$" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label data-error="wrong" data-success="right" for="lName">Last name</label>
                        </div>
                    </div>
                    <div class="md-form mb-5">
                        <div class="group">
                            <input type="text" class="putName" name="login" id="login" value="${patient.login}"
                                   pattern="^[a-zA-Z][a-zA-Z0-9\\-\\_\\.]{1,18}[a-zA-Z0-9]$" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label data-error="wrong" data-success="right" for="login">Login</label>
                        </div>
                    </div>
                    <div class="md-form mb-5">
                        <div class="group">
                            <input type="text" class="putName" name="phone" id="phone" value="${patient.phone}"
                                   pattern="^\+380\d{3}\d{2}\d{2}\d{2}$" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label data-error="wrong" data-success="right" for="phone">Phone</label>
                        </div>
                    </div>
                    <div class="md-form mb-5">
                        <div class="group">
                            <input name="dateB" class="max-today" type="date" id="dateB" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <input type="submit" class="action-button" value="Update" style="width: 30%">
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalChangeForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Change password</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="${pageContext.request.contextPath}/changePassPatient">
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <div class="group">
                            <input type="password" class="putName" id="currPass" name="currentPass"
                                   pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label data-error="wrong" data-success="right" for="currPass">Current password</label>
                        </div>
                    </div>
                    <div class="md-form mb-5">
                        <div class="group">
                            <input type="password" class="putName" name="newPass" id="newPass"
                                   pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label data-error="wrong" data-success="right" for="newPass">New password</label>
                        </div>
                    </div>
                    <div class="md-form mb-5">
                        <div class="group">
                            <input type="password" class="putName" name="newConfPass" id="newConfPass"
                                   pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label data-error="wrong" data-success="right" for="newConfPass">Confirm new
                                password</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <input type="submit" class="action-button" value="Change" style="width: 30%">
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalDeleteForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Delete profile</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <label>Are you sure you want to delete your account?</label>
            </div>
            <form method="post" action="${pageContext.request.contextPath}/deletePatient">
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <div class="group">
                            <input type="password" class="putName" name="currentPass" id="currentPass"
                                   pattern="^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$" required>
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label data-error="wrong" data-success="right" for="currentPass">Current password</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <input type="submit" class="action-button" value="VES" style="width: 45%">
                    <input type="button" class="action-button" data-dismiss="modal" style="width: 45%" value="NO">
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    setTimeout(function () {
        $('.boxHidden').fadeOut('fast')
    }, 3000);

    jQuery('img.svg').each(function () {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function (data) {
            var $svg = jQuery(data).find('svg');
            if (typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            if (typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass + ' replaced-svg');
            }
            $svg = $svg.removeAttr('xmlns:a');
            $img.replaceWith($svg);

        }, 'xml');

    });

    $(document).on("click", ".modalUpd", function () {
        document.getElementById("dateB").value = "${patient.dateOfBirth}";
        var today = new Date();
        var min = new Date();
        var dd = today.getDate() - 1;
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        min = (yyyy - 120) + '-' + mm + '-' + dd;
        today = yyyy + '-' + mm + '-' + dd;
        document.getElementById("dateB").setAttribute("max", today);
        document.getElementById("dateB").setAttribute("min", min);
    });

</script>
</body>

</html>


package model;

import validation.DateBirth;
import org.joda.time.LocalDate;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Objects;

public class Patient extends User {
    private static final String PHONE_REGEX = "^\\+380\\d{3}\\d{2}\\d{2}\\d{2}$";
    @DateBirth
    @NotNull(message = "Date of Birth cannot be null")
    private LocalDate dateOfBirth;

    @Pattern(regexp = PHONE_REGEX)
    @NotNull(message = "Phone cannot be null")
    private String phone;

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "id=" + this.getId() +
                ", firstName='" + this.getFirstName() + '\'' +
                ", lastName='" + this.getLastName() + '\'' +
                ", login='" + this.getLogin() + '\'' +
                ", password='" + this.getPassword() + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Patient)) return false;
        Patient patient = (Patient) o;
        return Objects.equals(this.getId(), patient.getId()) &&
                Objects.equals(this.getFirstName(), patient.getFirstName()) &&
                Objects.equals(this.getLastName(), patient.getLastName());
    }

    @Override
    public int hashCode() {

        return Objects.hash(this.getFirstName(), this.getLastName(), this.getLogin(), dateOfBirth, phone);
    }
}

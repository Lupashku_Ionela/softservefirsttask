package model;

import org.joda.time.LocalTime;

import java.util.Objects;
import java.util.UUID;

import org.joda.time.format.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


@validation.Record
public class Record {
    private UUID id;

    @Valid
    private Doctor doctor;

    @Valid
    private Patient patient;

    @NotNull(message = "Start time cannot be null")
    private LocalTime start;

    @NotNull(message = "End time cannot be null")
    private LocalTime end;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }

    public Doctor getDoctor() {
        return doctor;
    }
    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Patient getPatient() {
        return patient;
    }
    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public void setStart(LocalTime time){
        this.start=time;
    }
    public  LocalTime getStart(){
        return  this.start;
    }
    public void setEnd(LocalTime time){
        this.end=time;
    }
    public  LocalTime getEnd(){
        return  this.end;
    }

    public Record(){}
    public Record(LocalTime time, int duration){
        this.start=time;
        this.end=time.plusMinutes(duration);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Record)) return false;
        Record record = (Record) o;
        return Objects.equals(id, record.id) &&
                Objects.equals(doctor, record.doctor) &&
                Objects.equals(patient, record.patient) &&
                Objects.equals(start, record.start) &&
                Objects.equals(end, record.end);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, doctor, patient, start, end);
    }

    @Override
    public String toString() {
        return "Record{" +
                "id=" + id +
                ", start=" + start.toString(DateTimeFormat.forPattern("HH:mm")) +
                ", end=" + end.toString(DateTimeFormat.forPattern("HH:mm")) +
                '}';
    }
}


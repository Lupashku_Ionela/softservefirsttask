package model;

import org.joda.time.LocalTime;

import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@validation.Doctor
public class Doctor extends User {
    @NotNull(message = "Start time cannot be null")
    private LocalTime startTime;
    @NotNull(message = "End time cannot be null")
    private LocalTime endTime;

    private List<Record> records = new LinkedList<>();


    public void setStartTime(LocalTime time) {
        this.startTime = time;
    }

    public LocalTime getStartTime() {
        return this.startTime;
    }

    public void setEndTime(LocalTime time) {
        this.endTime = time;
    }

    public LocalTime getEndTime() {
        return this.endTime;
    }

    public void setRecords(List<Record> records) {
        this.records = (LinkedList<Record>) records;
    }

    public LinkedList<Record> getRecords() {
        return (LinkedList<Record>) this.records;
    }

    public void addRecord(int i, Record record) {
        this.records.add(i, record);
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + this.getId() +
                ", firstName='" + this.getFirstName() + '\'' +
                ", lastName='" + this.getLastName() + '\'' +
                ", login='" + this.getLogin() + '\'' +
                ", password='" + this.getPassword() + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", records=" + records +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Doctor)) return false;
        Doctor doctor = (Doctor) o;
        return Objects.equals(this.getId(), doctor.getId()) &&
                Objects.equals(this.getFirstName(), doctor.getFirstName()) &&
                Objects.equals(this.getLastName(), doctor.getLastName());
    }

    @Override
    public int hashCode() {

        return Objects.hash(this.getFirstName(), this.getLastName(), this.getLogin(), startTime, endTime, records);
    }
}


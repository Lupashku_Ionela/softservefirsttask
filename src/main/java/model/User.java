package model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Objects;
import java.util.UUID;

public class User {
    private static final String NAME_REGEX = "^[A-Z][a-z]{2,14}$";
    private static final String LOGIN_REGEX ="^[a-zA-Z][a-zA-Z0-9\\-\\_\\.]{1,18}[a-zA-Z0-9]$";
    private static final String PASSWORD_REGEX = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$";

    private UUID id;

    @Pattern(regexp = NAME_REGEX, message = "First name must match \"^[A-Z][a-z]{2,14}$\" ")
    @NotNull(message = "First name cannot be null")
    private String firstName;

    @Pattern(regexp = NAME_REGEX, message = "Last name must match \"^[A-Z][a-z]{2,14}$\" ")
    @NotNull(message = "Last name cannot be null")
    private String lastName;

    @Pattern(regexp = LOGIN_REGEX, message = "Login must match \"^[a-zA-Z][a-zA-Z0-9\\-\\_\\.]{1,18}[a-zA-Z0-9]$\" ")
    @NotNull(message = "Login cannot be null")
    private String login;

    @Pattern(regexp = PASSWORD_REGEX, message = "Login must match \"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$\" ")
    @NotNull(message = "Password cannot be null")
    private String password;

    private Role role;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(login, user.login) &&
                Objects.equals(password, user.password) &&
                role == user.role;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, login, password, role);
    }
}

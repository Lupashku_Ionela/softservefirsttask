package model;

public enum Role {
    PATIENT,
    DOCTOR,
    ADMIN
}
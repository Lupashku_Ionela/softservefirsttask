package service;

import dao.DoctorDAO;
import dao.RecordDAO;
import dao.UserDAO;
import encryption.BCrypter;
import exception.BadTimeException;
import exception.DeleteException;
import model.Doctor;
import model.Record;
import model.Role;
import model.User;
import validation.ScheduleValidation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.LocalTime;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DoctorService {
    private static final String USER_ALREADY_EXIST_MESSAGE = "User with this login already exist!";
    private static final String PASSWORDS_NOT_EQUAL = "Passwords are not equal!";

    private final Logger logger = LogManager.getLogger(DoctorService.class);

    public List<String> registerDoctor(String firstName, String lastName, LocalTime start_time,LocalTime end_time,
                                       String login, String password, String confirmPassword)
            throws SQLException, ClassNotFoundException, BadTimeException {
        List<String> errorMessages = new ArrayList<>();
        logger.info("Register doctor");
        if (!password.equals(confirmPassword)) {
            logger.error(PASSWORDS_NOT_EQUAL);
            errorMessages.add(PASSWORDS_NOT_EQUAL);
            return errorMessages;
        }

        if (getUserFromDB(login)) {
            logger.error(USER_ALREADY_EXIST_MESSAGE);
            errorMessages.add(USER_ALREADY_EXIST_MESSAGE);
            return errorMessages;
        }

        Doctor doctor = new Doctor();
        doctor.setFirstName(firstName);
        doctor.setLastName(lastName);
        doctor.setLogin(login);
        doctor.setPassword(BCrypter.getHashPassword(password));
        doctor.setStartTime(start_time);
        doctor.setEndTime(end_time);

        errorMessages = new ScheduleValidation().validate(doctor);

        if(errorMessages.isEmpty()) {
            logger.info("Add doctor to DB");
            new DoctorDAO().addDoctor(doctor);
        }
        else logger.error(errorMessages);
        return errorMessages;
    }

    private boolean getUserFromDB(String login) throws SQLException, ClassNotFoundException, BadTimeException {
        return new UserDAO().isUserInDB(login);
    }

    public Doctor getDoctorById(UUID id) throws SQLException, ClassNotFoundException, BadTimeException {
        return new DoctorDAO().getDoctorById(id);
    }

    public List<String> updateDoctorInfo(Doctor currentDoctor, String firstName, String lastName,
                                         String login, LocalTime startTime, LocalTime endTime) throws SQLException, ClassNotFoundException, BadTimeException {
        List<String> errors = new ArrayList<>();
        if(!currentDoctor.getLogin().equals(login)) {
            if (getUserFromDB(login)) {
                logger.error(USER_ALREADY_EXIST_MESSAGE);
                errors.add(USER_ALREADY_EXIST_MESSAGE);
                return errors;
            }
        }
        currentDoctor.setFirstName(firstName);
        currentDoctor.setLastName(lastName);
        currentDoctor.setLogin(login);
        currentDoctor.setStartTime(startTime);
        currentDoctor.setEndTime(endTime);

        errors = new ScheduleValidation().validate(currentDoctor);
        User user=new User();
        user.setLogin(login);
        user.setId(currentDoctor.getId());
        user.setPassword(currentDoctor.getPassword());
        user.setRole(Role.DOCTOR);
        if (!errors.isEmpty()) {
            logger.error(errors);
            return errors;
        }
        new UserDAO().updateUserById(user);
        new DoctorDAO().updateDoctorById(currentDoctor);

        return errors;
    }

    public List<Record> getAllRecords(Doctor doctor) throws SQLException, ClassNotFoundException, BadTimeException {
        List<Record> records = new ArrayList<>();
        records=new RecordDAO().getRecordsByDoctor(doctor);
        return records;
    }

    public List<String> updateDoctorPassword(Doctor currentDoctor, String newPassword, String confirmNew, String currentPassword) throws SQLException, ClassNotFoundException {
        List<String> errors = new ArrayList<>();

        if (!newPassword.equals(confirmNew)) {
            logger.error("Passwords do not match!!!");
            errors.add("Passwords do not match!!!");
            return errors;
        } else if (BCrypter.checkPassword(currentPassword, currentDoctor.getPassword())) {
            logger.error("Current password is wrong!");
            errors.add("Current password is wrong!");
            return errors;
        }

        currentDoctor.setPassword(BCrypter.getHashPassword(newPassword));
        errors = new ScheduleValidation().validate(currentDoctor);

        if (!errors.isEmpty()) {
            logger.error(errors);
            return errors;
        }
        User user=new User();
        user.setId(currentDoctor.getId());
        user.setLogin(currentDoctor.getLogin());
        user.setRole(Role.DOCTOR);
        new UserDAO().updateUserById(user);
        return null;
    }

    public List<String> deleteDoctor(Doctor currentDoctor, String currentPassword) throws SQLException, DeleteException, ClassNotFoundException {
        List<String> errors = new ArrayList<>();

        if (BCrypter.checkPassword(currentPassword, currentDoctor.getPassword())) {
            logger.error("Current password is wrong!");
            errors.add("Current password is wrong!");
            return errors;
        }
        new UserDAO().makeUserPassiveById(currentDoctor.getId());
        return errors;
    }


    public List<Doctor> getAllDoctors() throws SQLException, ClassNotFoundException {
        List<Doctor> doctors = new DoctorDAO().getAllDoctors();
        if (doctors == null || doctors.isEmpty())
            return null;

        return doctors;

    }

    public List<Record> freeRanges(UUID id) throws SQLException, BadTimeException, ClassNotFoundException {
        Doctor doctor=new DoctorDAO().getDoctorById(id);
        return new Visit().getFreeTimesDoctor(doctor);
    }
}

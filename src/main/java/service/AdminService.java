package service;

import dao.DoctorDAO;
import dao.PatientDAO;
import exception.BadTimeException;
import model.Doctor;
import model.Patient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;

public class AdminService {
    private final Logger logger = LogManager.getLogger(AdminService.class);

    public List<Doctor> getAllActiveDoctors() throws SQLException, ClassNotFoundException, BadTimeException {
        List<Doctor> activeDoctors = new DoctorDAO().getAllActiveDoctors();
        logger.info("Active doctors: "+activeDoctors);
        return activeDoctors;
    }

    public List<Doctor> getAllDisabledDoctors() throws SQLException, ClassNotFoundException, BadTimeException {
        List<Doctor> disabledDoctors = new DoctorDAO().getAllDisabledDoctors();
        logger.info("Disabled doctors: "+disabledDoctors);
        return disabledDoctors;
    }

    public List<Patient> getAllActivePatients() throws SQLException, ClassNotFoundException {
        List<Patient> activePatients = new PatientDAO().getAllActivePatients();
        logger.info("Active patients: "+activePatients);
        return activePatients;
    }

    public List<Patient> getAllDisabledPatients() throws SQLException, ClassNotFoundException {
        List<Patient> disabledPatients = new PatientDAO().getAllDisabledPatients();
        logger.info("Disabled patients: "+disabledPatients);
        return disabledPatients;

    }
}

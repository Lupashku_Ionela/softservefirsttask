package service;

import dao.DoctorDAO;
import dao.PatientDAO;
import dao.RecordDAO;
import dao.UserDAO;
import encryption.BCrypter;
import exception.BadTimeException;
import exception.DeleteException;
import model.*;
import validation.ScheduleValidation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class PatientService {

    private static final String USER_ALREADY_EXIST_MESSAGE = "User with this login already exist!";
    private static final String PASSWORDS_NOT_EQUAL = "Passwords are not equal!";
    private final Logger logger = LogManager.getLogger(PatientService.class);
    public List<String> registerPatient(String firstName, String lastName, LocalDate date, String phone, String login,
                                        String password, String confirmPassword) throws SQLException, ClassNotFoundException, BadTimeException {
        List<String> errorMessages = new ArrayList<>();
        logger.info("Register patient");
        if (!password.equals(confirmPassword)) {
            logger.error(PASSWORDS_NOT_EQUAL);
            errorMessages.add(PASSWORDS_NOT_EQUAL);
            return errorMessages;
        }

        if (getUserFromDB(login)) {
            logger.error(USER_ALREADY_EXIST_MESSAGE);
            errorMessages.add(USER_ALREADY_EXIST_MESSAGE);
            return errorMessages;
        }

        Patient patient = new Patient();
        patient.setFirstName(firstName);
        patient.setLastName(lastName);
        patient.setLogin(login);
        patient.setDateOfBirth(date);
        patient.setPhone(phone);
        patient.setPassword(BCrypter.getHashPassword(password));

        errorMessages = new ScheduleValidation().validate(patient);

        if(errorMessages.isEmpty()) {
            logger.info("Add doctor to DB");
            new PatientDAO().addPatient(patient);
        }
        else logger.error(errorMessages);

        return errorMessages;
    }

    private boolean getUserFromDB(String login) throws SQLException, ClassNotFoundException, BadTimeException {
        return new UserDAO().isUserInDB(login);
    }
    public Patient getPatientByLogin(String login) throws SQLException, ClassNotFoundException {
        return new PatientDAO().getPatientByLogin(login);
    }
    public Patient getPatientById(UUID id) throws SQLException, ClassNotFoundException {
        return new PatientDAO().getPatientById(id);
    }

    public List<String> updatePatientInfo(Patient currentPatient, String firstName, String lastName, String login, String phone, LocalDate date) throws SQLException, ClassNotFoundException, BadTimeException {

        List<String> errors = new LinkedList<>();
        if((!currentPatient.getLogin().equals(login))&&(getUserFromDB(login))) {
                logger.error(USER_ALREADY_EXIST_MESSAGE);
                errors.add(USER_ALREADY_EXIST_MESSAGE);
                return errors;
        }
        currentPatient.setFirstName(firstName);
        currentPatient.setLastName(lastName);

        currentPatient.setDateOfBirth(date);
        currentPatient.setPhone(phone);
        currentPatient.setLogin(login);
        errors = new ScheduleValidation().validate(currentPatient);
        User user=new User();
        user.setLogin(login);
        user.setId(currentPatient.getId());
        user.setPassword(currentPatient.getPassword());
        user.setRole(Role.PATIENT);
        if (!errors.isEmpty()) {
            logger.error(errors);
            return errors;
        }
        new UserDAO().updateUserById(user);
        new PatientDAO().updatePatientById(currentPatient);

        return errors;
    }

    public List<String> updatePatientPassword(Patient currentPatient, String newPassword, String confirmNew, String currentPassword) throws SQLException, ClassNotFoundException {
        List<String> errors = new ArrayList<>();

        if (!newPassword.equals(confirmNew)) {
            logger.error("Passwords do not match!!!!");
            errors.add("Passwords do not match!!!");
            return errors;
        } else if (BCrypter.checkPassword(currentPassword, currentPatient.getPassword())) {
            logger.error("Current password is wrong!");
            errors.add("Current password is wrong!");
            return errors;
        }

        currentPatient.setPassword(BCrypter.getHashPassword(newPassword));
        errors = new ScheduleValidation().validate(currentPatient);

        if (!errors.isEmpty()){
            logger.error(errors);
            return errors;
        }

        User user=new User();
        user.setId(currentPatient.getId());
        user.setLogin(currentPatient.getLogin());
        user.setRole(Role.PATIENT);
        new UserDAO().updateUserById(user);

        return errors;
    }

    public List<String> deletePatient(Patient currentPatient, String currentPassword) throws SQLException, DeleteException, ClassNotFoundException {
        List<String> errors = new ArrayList<>();

        if (BCrypter.checkPassword(currentPassword, currentPatient.getPassword())) {
            logger.error("Current password is wrong!");
            errors.add("Current password is wrong!");
            return errors;
        }
        new UserDAO().makeUserPassiveById(currentPatient.getId());
        return errors;
    }

    public List<Doctor> getAllActiveDoctors() throws SQLException, ClassNotFoundException, BadTimeException {
        List<Doctor> doctors=new ArrayList<>();
             doctors= new DoctorDAO().getAllActiveDoctors();
        return doctors;
    }

    public List<Record> getAllRecords(Patient patient) throws SQLException, ClassNotFoundException, BadTimeException {
        List<Record> records = new ArrayList<>();
             records=   new RecordDAO().getRecordsByPatient(patient);
        return records;
    }

    public List<LocalTime> freeTimesForDuration(UUID id_doctor, LocalTime start, int duration) throws SQLException, BadTimeException, ClassNotFoundException {
        List<String> errorMessages = new ArrayList<>();
        Doctor doctor=new DoctorDAO().getDoctorById(id_doctor);
        Record record=new Record(start,duration);
        errorMessages = new ScheduleValidation().validate(record);

        List<LocalTime> times = new ArrayList<>();
        if(errorMessages.isEmpty()) {
            times=new Visit().getFreeTimes(doctor,record);
        }
        else logger.error(errorMessages);
        return times;
    }

    public List<String> addRecord(Patient currentPatient, UUID id_doctor, LocalTime start, int duration) throws SQLException, ClassNotFoundException, BadTimeException {
        List<String> errorMessages = new ArrayList<>();

        Doctor doctor=new DoctorDAO().getDoctorById(id_doctor);

        Record record=new Record(start,duration);
        record.setPatient(currentPatient);
        errorMessages = new ScheduleValidation().validate(record);
        LinkedList<Record> patientRecords=new RecordDAO().getRecordsByPatient(currentPatient);

        if(errorMessages.isEmpty()) {
            try {
                new Visit().addRecord(doctor, record);
                record.setDoctor(doctor);
                new Visit().addRecordPatient(patientRecords,record);
                new RecordDAO().addRecord(record);
            } catch (BadTimeException e) {
                errorMessages.add(e.getMessage());
                logger.error(errorMessages);
                return errorMessages;
            }
        }
        return errorMessages;
    }

}

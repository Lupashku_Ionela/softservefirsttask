package service;

import exception.BadTimeException;
import model.Doctor;
import model.Record;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static java.lang.Math.abs;

public class Visit  {
    private final Logger logger = LogManager.getLogger(Visit.class);
    public boolean addRecord(Doctor doctor, Record record)throws BadTimeException {
        logger.info("Add record\n"+doctor.toString()+"\n"+record.toString());
        if (record.getStart().isBefore(doctor.getStartTime())
                ||record.getEnd().isBefore(doctor.getStartTime())) {
            logger.error("Too early");
            throw new BadTimeException("Too early");
        }
        if (record.getEnd().isAfter(doctor.getEndTime())) {
            logger.error("Too late");
            throw new BadTimeException("Too late");
        }
        if (doctor.getRecords().isEmpty()) {
            doctor.addRecord(0,record);
            logger.info("Successful add at "+0+" position");
            return true; }
            if (record.getEnd().isBefore(doctor.getRecords().getFirst().getStart().plusMinutes(1))) {
                doctor.addRecord(0,record);
                logger.info("Successful add at "+0+" position");
                return true;
                }
                if(record.getStart().isAfter(doctor.getRecords().getLast().getEnd().minusMinutes(1))) {
                    logger.info("Successful add at "+1+" position");
                    doctor.addRecord(1, record);
                    return true;
                    }
                    Iterator<Record> it = doctor.getRecords().iterator();
                    Record record1=doctor.getRecords().getFirst();
                    Record next;
                    int i=0;
                    it.next();
                    while(it.hasNext()){
                        next=it.next();
                        ++i;
                        if ((record.getStart().isAfter(record1.getEnd().minusMinutes(1))
                                && record.getEnd().isBefore(next.getStart().plusMinutes(1)))) {
                            doctor.addRecord(i,record);
                            logger.info("Successful add at "+i+" position");
                            return true;
                            }
                            record1=next;
                        }
                        logger.error("The doctor is busy at this time");
                        throw new BadTimeException("The doctor is busy at this time");
    }

    public boolean addRecordPatient(LinkedList<Record> records, Record record)throws BadTimeException {
        logger.info("Add record\n"+record.toString());
        if (records.isEmpty()) {
            logger.info("Successful add at "+0+" position");
            return true; }
        if (record.getEnd().isBefore(records.getFirst().getStart().plusMinutes(1))) {
            logger.info("Successful add at "+0+" position");
            return true;
        }
        if(record.getStart().isAfter(records.getLast().getEnd().minusMinutes(1))) {
            logger.info("Successful add at "+1+" position");
            return true;
        }
        Iterator<Record> it = records.iterator();
        Record record1=records.getFirst();
        Record next;
        int i=0;
        it.next();
        while(it.hasNext()){
            next=it.next();
            ++i;
            if ((record.getStart().isAfter(record1.getEnd().minusMinutes(1))
                    && record.getEnd().isBefore(next.getStart().plusMinutes(1)))) {
                logger.info("Successful add at "+i+" position");
                return true;
            }
            record1=next;
        }
        logger.error("You are busy at this time");
        throw new BadTimeException("You are busy at this time");
    }

    public int differenceTimes(LocalTime first, LocalTime second){
            Minutes m = Minutes.minutesBetween(first, second);
            return abs(m.getMinutes());
    }

    public List<Record> getFreeTimesDoctor(Doctor doctor){
        List<Record> res=new ArrayList<>();
        logger.info("Free times for "+doctor);
        if(!doctor.getRecords().isEmpty()) {
            if (differenceTimes(doctor.getStartTime(), doctor.getRecords().getFirst().getStart()) >= 2) {
                Record record = new Record();
                record.setStart(doctor.getStartTime());
                record.setEnd(doctor.getRecords().getFirst().getStart());
                res.add(record);
            }
            Iterator<Record> it = doctor.getRecords().iterator();
            Record record1 = doctor.getRecords().getFirst();
            Record next;
            it.next();
            while (it.hasNext()) {
                next = it.next();
                if (differenceTimes(record1.getEnd(), next.getStart()) >= 2) {
                    Record record = new Record();
                    record.setStart(record1.getEnd());
                    record.setEnd(next.getStart());
                    res.add(record);
                }
                record1 = next;
            }
            if (differenceTimes(doctor.getRecords().getLast().getEnd(), doctor.getEndTime()) >= 2) {
                Record record = new Record();
                record.setStart(doctor.getRecords().getLast().getEnd());
                record.setEnd(doctor.getEndTime());
                res.add(record);
            }
        }
        else if(differenceTimes( doctor.getStartTime(), doctor.getEndTime())>= 2){
                Record record=new Record();
                record.setStart(doctor.getStartTime());
                record.setEnd( doctor.getEndTime());
                res.add(record);

        }
        logger.info("Result free times:"+res);
        return res;
    }


    public List<LocalTime> getFreeTimes(Doctor doctor, Record record){
        List<LocalTime> res=new ArrayList<>();
        int interval=differenceTimes(record.getEnd(),record.getStart());
        logger.info("Free times for "+record+"   interval="+interval+"\n"+doctor);
        if(!doctor.getRecords().isEmpty()) {
            if (differenceTimes(doctor.getStartTime(), doctor.getRecords().getFirst().getStart()) >= interval) {
                res.add(doctor.getStartTime());
            }
            if (differenceTimes(doctor.getRecords().getLast().getEnd(), doctor.getEndTime()) >= interval) {
                res.add(doctor.getRecords().getLast().getEnd());
            }
            Iterator<Record> it = doctor.getRecords().iterator();
            Record record1 = doctor.getRecords().getFirst();
            Record next;
            it.next();
            while (it.hasNext()) {
                next = it.next();
                if (differenceTimes(record1.getEnd(), next.getStart()) >= interval) {
                    res.add(record1.getEnd());
                }
                record1 = next;
            }
            res.sort(Comparator.comparingInt(o -> abs(Minutes.minutesBetween(o, record.getStart()).getMinutes())));
        }
        else res.add(doctor.getStartTime());
        logger.info("Result free times:"+res);
        return res;
    }


    public LocalTime nearestTime(Doctor doctor, Record record){
        logger.info("Nearest time:  ");
        return getFreeTimes(doctor,record).get(0);
    }

}

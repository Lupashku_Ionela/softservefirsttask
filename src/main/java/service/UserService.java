package service;

import dao.RecordDAO;
import encryption.BCrypter;
import exception.BadTimeException;
import exception.DeleteException;
import model.Record;
import model.User;
import dao.UserDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserService {
    private final int minRevokeRecordTime=15;
    private final Logger logger = LogManager.getLogger(UserService.class);
    public String loginUserByPassword(String login, String password) throws SQLException, ClassNotFoundException {
        User user = new UserDAO().getUserByLogin(login);
        System.out.println(user);
        if(!new UserDAO().isUserActive(login)|| user == null || BCrypter.checkPassword(password, user.getPassword())) {
            logger.error("Login or password is incorrect!");
            return "Login or password is incorrect!";
        }
        return null;
    }

    public User getUserByLogin(String login) throws SQLException, ClassNotFoundException {
        return new UserDAO().getUserByLogin(login);
    }
    public User getUserById(UUID id) throws SQLException, ClassNotFoundException {
        return new UserDAO().getUserById(id);
    }
    public void changeUserStatus(UUID id,boolean status) throws DeleteException, SQLException, ClassNotFoundException {
        if(status) new UserDAO().makeUserPassiveById(id);
        else new UserDAO().makeUserActiveById(id);
    }
    public List<String> revokeRecord(UUID idRecord) throws SQLException, DeleteException, ClassNotFoundException, BadTimeException {
        List<String> errors = new ArrayList<>();
        Record record=new RecordDAO().getRecordById(idRecord);
        logger.info("Revoke :"+record.toString());
        LocalTime localTime = new LocalTime();
        if((Minutes.minutesBetween(localTime,record.getStart()).getMinutes()<minRevokeRecordTime)&&(localTime.isBefore(record.getStart()))){
            errors.add("You cant revoke appointment less then 15 minutes before");
        }
        if(!errors.isEmpty()){
            logger.error(errors);
            return errors;
        }
        new RecordDAO().deleteRecordById(idRecord);

        return errors;
    }
}


package dao;

import dao.interfaces.PatientIDao;
import dataBase.DataBaseConnection;
import exception.DeleteException;
import model.Patient;
import model.Role;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.LocalDate;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PatientDAO implements PatientIDao {
    private final Logger logger = LogManager.getLogger(PatientDAO.class);

    @Override
    public Patient getPatientById(UUID id) throws SQLException, ClassNotFoundException {
        logger.info("Try to get patient by id " + id);
        Connection conn = DataBaseConnection.getNewConnection();
        String prepQuerry = "SELECT * FROM patient INNER JOIN users ON patient.id = users.id WHERE patient.id=?";
        PreparedStatement preparedStatement = conn.prepareStatement(prepQuerry);
        preparedStatement.setObject(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        Patient patient = getPatientFromResultSet(resultSet);
        assert patient != null;
        logger.info("Get " + patient.toString());
        return patient;
    }

    @Override
    public Patient getPatientByLogin(String login) throws ClassNotFoundException, SQLException {
        logger.info("Try to get patient with login " + login);
        Connection conn = DataBaseConnection.getNewConnection();
        String prepQuerry = "SELECT * FROM patient INNER JOIN users ON patient.id = users.id WHERE login=?";
        PreparedStatement preparedStatement = conn.prepareStatement(prepQuerry);
        preparedStatement.setString(1, login);
        ResultSet resultSet = preparedStatement.executeQuery();
        Patient patient = getPatientFromResultSet(resultSet);
        assert patient != null;
        logger.info("Get " + patient.toString());
        return patient;
    }

    private Patient getPatientFromResultSet(ResultSet resultSet) throws SQLException {
        Patient patient = null;
        if (resultSet.next()) {
            patient = new Patient();
            patient.setId((UUID) resultSet.getObject("id"));
            patient.setFirstName(resultSet.getString("first_name"));
            patient.setLastName(resultSet.getString("last_name"));
            patient.setDateOfBirth(new LocalDate(resultSet.getObject("date_of_birth")));
            patient.setPhone(resultSet.getString("phone"));
            patient.setLogin(resultSet.getString("login"));
            patient.setPassword(resultSet.getString("password"));
            patient.setRole(Role.PATIENT);
        }
        return patient;
    }

    @Override
    public void addPatient(Patient patient) throws ClassNotFoundException, SQLException {
        logger.info("Try to add " + patient.toString());
        if (patient.getId() == null) {
            Connection conn = DataBaseConnection.getNewConnection();

            User user = new User();
            user.setId(patient.getId());
            user.setLogin(patient.getLogin());
            user.setPassword(patient.getPassword());
            user.setRole(Role.PATIENT);
            UUID uuid = new UserDAO().addUser(user);
            String preparedQuerry = "INSERT INTO patient (id,first_name,last_name,date_of_birth," +
                    "phone) VALUES(?,?,?,?,?)";
            PreparedStatement preparedStmt = conn.prepareStatement(preparedQuerry);
            preparedStmt.setObject(1, uuid);
            preparedStmt.setString(2, patient.getFirstName());
            preparedStmt.setString(3, patient.getLastName());
            preparedStmt.setDate(4, new Date(patient.getDateOfBirth().getYear() - 1900,
                    patient.getDateOfBirth().getMonthOfYear() - 1, patient.getDateOfBirth().getDayOfMonth()));
            preparedStmt.setString(5, patient.getPhone());
            preparedStmt.executeUpdate();

            logger.info("Added " + patient.toString());
        }
    }

    @Override
    public void deletePatientById(UUID id) throws SQLException, ClassNotFoundException, DeleteException {
        logger.info("Try to delete patient by id " + id);
        Connection conn = DataBaseConnection.getNewConnection();
        String prepDelete = "DELETE FROM patient WHERE id= ?;";
        String prepQuery = "SELECT id FROM records WHERE id_patient = ?;";
        PreparedStatement preparedStatement = conn.prepareStatement(prepDelete);
        preparedStatement.setObject(1, id);

        PreparedStatement preparedStatement1 = conn.prepareStatement(prepQuery);
        preparedStatement1.setObject(1, id);
        ResultSet resultSet = preparedStatement1.executeQuery();
        if (resultSet.getFetchSize() != 0) {
            logger.error("Patient " + id + " has records");
            throw new DeleteException("Patient " + id + " has records");
        }
        int rs = preparedStatement.executeUpdate();
        conn.close();
        if (rs == 0) {
            logger.error("Patient " + id + " not found");
            throw new IllegalArgumentException("Patient " + id + " not found");
        }
        logger.info("Deleted patient: " + id);

    }

    @Override
    public void updatePatientById(Patient patient) throws SQLException, ClassNotFoundException {
        logger.info("Try to update patient by id " + patient.toString());
        Connection conn = DataBaseConnection.getNewConnection();
        String prepUpdate = "UPDATE patient SET first_name=?,last_name =?,date_of_birth=?,phone=?" +
                " WHERE id=?;";
        PreparedStatement preparedStatement = conn.prepareStatement(prepUpdate);
        preparedStatement.setString(1, patient.getFirstName());
        preparedStatement.setString(2, patient.getLastName());
        preparedStatement.setDate(3, new Date(patient.getDateOfBirth().getYear() - 1900,
                patient.getDateOfBirth().getMonthOfYear() - 1, patient.getDateOfBirth().getDayOfMonth()));
        preparedStatement.setString(4, patient.getPhone());
        preparedStatement.setObject(5, patient.getId());
        int res = preparedStatement.executeUpdate();
        conn.close();
        if (res == 0) {
            logger.error(patient.toString() + " not found");
            throw new IllegalArgumentException(patient.toString() + " not found");
        }
        logger.info("Updated : " + patient.toString());

    }

    @Override
    public List<Patient> getAllPatients() throws SQLException, ClassNotFoundException {
        logger.info("Try to get all patients ");
        List<Patient> patients = new ArrayList<>();
        Connection conn = DataBaseConnection.getNewConnection();
        String prepQuerry = "SELECT * FROM patient INNER JOIN users ON patient.id=users.id";
        PreparedStatement preparedStatement = conn.prepareStatement(prepQuerry);
        ResultSet resultSet = preparedStatement.executeQuery();
        conn.close();
        while (resultSet.next()) {
            Patient patient = new Patient();
            patient.setId((UUID) resultSet.getObject("id"));
            patient.setFirstName(resultSet.getString("first_name"));
            patient.setLastName(resultSet.getString("last_name"));
            patient.setDateOfBirth(new LocalDate(resultSet.getObject("date_of_birth")));
            patient.setPhone(resultSet.getString("phone"));
            patient.setLogin(resultSet.getString("login"));
            patient.setPassword(resultSet.getString("password"));
            patient.setRole(Role.PATIENT);
            patients.add(patient);
        }
        logger.info("All patients: " + patients.toString());
        return patients;
    }

    @Override
    public List<Patient> getAllActivePatients() throws ClassNotFoundException, SQLException {
        logger.info("Try to get all active patients ");
        List<Patient> patients = new ArrayList<>();
        Connection conn = DataBaseConnection.getNewConnection();
        String prepQuerry = "SELECT * FROM users INNER JOIN patient ON users.id=patient.id WHERE role=? AND active=TRUE ";
        PreparedStatement preparedStatement = conn.prepareStatement(prepQuerry);
        preparedStatement.setString(1, Role.PATIENT.toString());
        ResultSet resultSet = preparedStatement.executeQuery();
        patients=resultSetToList(resultSet);
        conn.close();
        logger.info("All active patients: " + patients.toString());
        return patients;
    }

    @Override
    public List<Patient> getAllDisabledPatients() throws ClassNotFoundException, SQLException {
        logger.info("Try to get all disabled patients ");
        List<Patient> patients = new ArrayList<>();
        Connection conn = DataBaseConnection.getNewConnection();
        String prepQuerry = "SELECT * FROM users INNER JOIN patient ON users.id=patient.id WHERE role=? AND active=FALSE ";
        PreparedStatement preparedStatement = conn.prepareStatement(prepQuerry);
        preparedStatement.setString(1, Role.PATIENT.toString());
        ResultSet resultSet = preparedStatement.executeQuery();
        patients=resultSetToList(resultSet);
        conn.close();
        conn.close();
        logger.info("All disabled patients: " + patients.toString());
        return patients;
    }

    private List<Patient> resultSetToList(ResultSet resultSet) throws SQLException {
        List<Patient> patients =null;
        while (resultSet.next()) {
            Patient patient = new Patient();
            patient.setRole(Role.PATIENT);
            patient.setId((UUID) resultSet.getObject("id"));
            patient.setLogin(resultSet.getString("login"));
            patient.setPassword(resultSet.getString("password"));
            patient.setFirstName(resultSet.getString("first_name"));
            patient.setLastName(resultSet.getString("last_name"));
            patient.setDateOfBirth(new LocalDate(resultSet.getObject("date_of_birth")));
            patient.setPhone(resultSet.getString("phone"));
            patients.add(patient);
        }
        return patients;
    }


}

package dao;

import dao.interfaces.DoctorIDao;
import dataBase.DataBaseConnection;
import exception.BadTimeException;
import exception.DeleteException;
import model.Doctor;
import model.Record;
import model.Role;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.LocalTime;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DoctorDAO implements DoctorIDao {
    private final Logger logger = LogManager.getLogger(DoctorDAO.class);


    @Override
    public Doctor getDoctorById(UUID id) throws ClassNotFoundException, SQLException, BadTimeException {
        logger.info("Try to get doctor by id " + id);
        Connection conn = DataBaseConnection.getNewConnection();
        String prepQuerry = "SELECT * FROM doctor INNER JOIN users ON doctor.id = users.id WHERE doctor.id=?";
        PreparedStatement preparedStatement = conn.prepareStatement(prepQuerry);
        preparedStatement.setObject(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        Doctor doctor = null;
        if (resultSet.next()) {
            doctor=new Doctor();
            doctor.setId((UUID) resultSet.getObject("id"));
            doctor.setFirstName(resultSet.getString("first_name"));
            doctor.setLastName(resultSet.getString("last_name"));
            doctor.setStartTime(new LocalTime(resultSet.getObject("start_time")));
            doctor.setEndTime(new LocalTime(resultSet.getObject("end_time")));
            doctor.setLogin(resultSet.getString("login"));
            doctor.setPassword(resultSet.getString("password"));
            doctor.setRole(Role.valueOf(resultSet.getString("role")));
            List<Record> records = new RecordDAO().getRecordsByDoctor(doctor);
            doctor.setRecords(records);
        }
        conn.close();
        assert doctor != null;
        logger.info("Get "+doctor.toString());
        return doctor;
    }

    @Override
    public Doctor getDoctorByIdWithoutRecords(UUID id) throws ClassNotFoundException, SQLException, BadTimeException {
        logger.info("Try to get doctor by id without records " + id);
        Connection conn = DataBaseConnection.getNewConnection();
        String prepQuerry = "SELECT * FROM doctor INNER JOIN users ON doctor.id = users.id WHERE doctor.id=?";
        PreparedStatement preparedStatement = conn.prepareStatement(prepQuerry);
        preparedStatement.setObject(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        Doctor doctor = getDoctorFromResultSet(resultSet);
        conn.close();
        assert doctor != null;
        logger.info("Get "+doctor.toString());
        return doctor;
    }

    private Doctor getDoctorFromResultSet(ResultSet resultSet) throws SQLException {
        Doctor doctor = null;
        if (resultSet.next()) {
            doctor=new Doctor();
            doctor.setId((UUID) resultSet.getObject("id"));
            doctor.setFirstName(resultSet.getString("first_name"));
            doctor.setLastName(resultSet.getString("last_name"));
            doctor.setStartTime(new LocalTime(resultSet.getObject("start_time")));
            doctor.setEndTime(new LocalTime(resultSet.getObject("end_time")));
            doctor.setLogin(resultSet.getString("login"));
            doctor.setPassword(resultSet.getString("password"));
            doctor.setRole(Role.valueOf(resultSet.getString("role")));
        }
        return doctor;
    }

    @Override
    public Doctor getDoctorByLogin(String login) throws ClassNotFoundException, SQLException, BadTimeException {
        logger.info("Try to get doctor with login "+login);
        Connection conn = DataBaseConnection.getNewConnection();
        String prepQuerry = "SELECT * FROM doctor INNER JOIN users ON doctor.id = users.id WHERE login=?";
        PreparedStatement preparedStatement = conn.prepareStatement(prepQuerry);
        preparedStatement.setString(1, login);
        ResultSet resultSet = preparedStatement.executeQuery();
        Doctor doctor =getDoctorFromResultSet(resultSet);
        conn.close();
        assert doctor != null;
        logger.info("Get "+doctor.toString());
        return doctor;
    }


    @Override
    public void addDoctor(Doctor doctor) throws ClassNotFoundException, SQLException {
        logger.info("Try to add " + doctor.toString());
        if (doctor.getId() == null) {
            Connection conn = DataBaseConnection.getNewConnection();
            Statement st = conn.createStatement();

            User user=new User();
            user.setId(doctor.getId());
            user.setLogin(doctor.getLogin());
            user.setPassword(doctor.getPassword());
            user.setRole(Role.DOCTOR);
            UUID uuid=new  UserDAO().addUser(user);
            logger.info("UUID " + uuid);
            String preparedQuerry1 = "INSERT INTO doctor (id,first_name,last_name,start_time,end_time) VALUES(?,?,?,?,?)";
            PreparedStatement preparedStmt = conn.prepareStatement(preparedQuerry1);
            preparedStmt.setObject(1,uuid);
            preparedStmt.setString(2, doctor.getFirstName());
            preparedStmt.setString(3, doctor.getLastName());
            preparedStmt.setTime(4, new Time(doctor.getStartTime().toDateTimeToday().getMillis()));
            preparedStmt.setTime(5, new Time(doctor.getEndTime().toDateTimeToday().getMillis()));
            preparedStmt.executeUpdate();

            logger.info("Added "+doctor.toString());
        }
    }

    @Override
    public void deleteDoctorById(UUID id) throws ClassNotFoundException, SQLException, DeleteException {
        logger.info("Try to delete doctor by id "+id);
        Connection conn = DataBaseConnection.getNewConnection();
        String prepDelete = "DELETE FROM doctor WHERE id= ?;";
        String prepQuery = "SELECT id FROM records WHERE id_doctor = ?;";
        PreparedStatement preparedStatement = conn.prepareStatement(prepDelete);
        preparedStatement.setObject(1, id);


        PreparedStatement preparedStatement1 = conn.prepareStatement(prepQuery);
        preparedStatement1.setObject(1, id);
        ResultSet resultSet = preparedStatement1.executeQuery();
        if (resultSet.getFetchSize() != 0) {
            logger.error("Doctor "+id+" has records");
            throw new DeleteException("Doctor "+id+" has records");
        }
        int rs = preparedStatement.executeUpdate();
        conn.close();
        if (rs == 0) {
            logger.error("Doctor "+id+" not found");
            throw new IllegalArgumentException("Doctor "+id+" not found");
        }
        logger.info("Deleted doctor: "+id);

    }

    @Override
    public void updateDoctorById(Doctor doctor) throws ClassNotFoundException, SQLException {
        logger.info("Try to update doctor by id "+doctor.toString());
        Connection conn = DataBaseConnection.getNewConnection();
        String prepUpdate = "UPDATE doctor SET first_name=?,last_name =?, start_time =?, end_time=?" +
                " WHERE id=?;";

        PreparedStatement preparedStatement = conn.prepareStatement(prepUpdate);
        preparedStatement.setString(1, doctor.getFirstName());
        preparedStatement.setString(2, doctor.getLastName());
        preparedStatement.setTime(3, new Time(doctor.getStartTime().toDateTimeToday().getMillis()));
        preparedStatement.setTime(4, new Time(doctor.getEndTime().toDateTimeToday().getMillis()));
        preparedStatement.setObject(5, doctor.getId());
        int res = preparedStatement.executeUpdate();
        conn.close();
        if (res == 0){
            logger.error(doctor.toString()+" not found");
            throw new IllegalArgumentException(doctor.toString()+" not found");
        }
        logger.info("Updated : "+doctor.toString());
    }

    @Override
    public List<Doctor> getAllDoctors() throws ClassNotFoundException, SQLException {
        logger.info("Try to get all doctors " );
        List<Doctor> doctors = new ArrayList<>();
        Connection conn = DataBaseConnection.getNewConnection();
        String prepQuerry = "SELECT * FROM doctor INNER JOIN users ON doctor.id=users.id";
        PreparedStatement preparedStatement = conn.prepareStatement(prepQuerry);
        ResultSet resultSet = preparedStatement.executeQuery();
        conn.close();
        while (resultSet.next()) {
            Doctor doctor = new Doctor();
            doctor.setId((UUID) resultSet.getObject("id"));
            doctor.setFirstName(resultSet.getString("first_name"));
            doctor.setLastName(resultSet.getString("last_name"));
            doctor.setStartTime(new LocalTime(resultSet.getObject("start_time")));
            doctor.setEndTime(new LocalTime(resultSet.getObject("end_time")));
            doctor.setLogin(resultSet.getString("login"));
            doctor.setPassword(resultSet.getString("password"));
            doctors.add(doctor);
        }
        logger.info("All doctors: " + doctors.toString());
        return doctors;
    }

    @Override
    public List<Doctor> getAllActiveDoctors() throws ClassNotFoundException, SQLException, BadTimeException {
        logger.info("Try to get all active doctors " );
        List<Doctor> doctors = new ArrayList<>();
        Connection conn = DataBaseConnection.getNewConnection();
        String prepQuerry = "SELECT * FROM users INNER JOIN doctor ON users.id=doctor.id WHERE role=? AND active=TRUE ";
        PreparedStatement preparedStatement = conn.prepareStatement(prepQuerry);
        preparedStatement.setString(1, Role.DOCTOR.toString());
        ResultSet resultSet = preparedStatement.executeQuery();
        doctors=resultSetToList(resultSet);
        conn.close();
        logger.info("All active doctors: " + doctors.toString());
        return doctors;
    }

    @Override
    public List<Doctor> getAllDisabledDoctors() throws ClassNotFoundException, SQLException, BadTimeException {
        logger.info("Try to get all disabled doctors " );
        List<Doctor> doctors;
        Connection conn = DataBaseConnection.getNewConnection();
        String prepQuerry = "SELECT * FROM users INNER JOIN doctor ON users.id=doctor.id WHERE role=? AND active=FALSE ";
        PreparedStatement preparedStatement = conn.prepareStatement(prepQuerry);
        preparedStatement.setString(1, Role.DOCTOR.toString());
        ResultSet resultSet = preparedStatement.executeQuery();
        doctors=resultSetToList(resultSet);
        conn.close();
        logger.info("All disabled doctors: " + doctors.toString());
        return doctors;
    }

    private List<Doctor> resultSetToList(ResultSet resultSet) throws SQLException {
        List<Doctor> doctors =new ArrayList<>() ;
        while (resultSet.next()) {
            Doctor doctor=new Doctor();
            doctor.setRole(Role.DOCTOR);
            doctor.setId((UUID) resultSet.getObject("id"));
            doctor.setLogin(resultSet.getString("login"));
            doctor.setPassword(resultSet.getString("password"));
            doctor.setFirstName(resultSet.getString("first_name"));
            doctor.setLastName(resultSet.getString("last_name"));
            doctor.setStartTime(new LocalTime(resultSet.getObject("start_time")));
            doctor.setEndTime(new LocalTime(resultSet.getObject("end_time")));
            doctors.add(doctor);
        }
        return doctors;
    }


}

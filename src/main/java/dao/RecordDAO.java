package dao;

import dao.interfaces.RecordIDao;
import dataBase.DataBaseConnection;
import exception.BadTimeException;
import model.Doctor;
import model.Patient;
import model.Record;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.LocalTime;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class RecordDAO implements RecordIDao {
    private final Logger logger = LogManager.getLogger(RecordDAO.class);
    @Override
    public Record getRecordById(UUID id) throws ClassNotFoundException, SQLException, BadTimeException {
        logger.info("Try to get record by id " + id);
        Connection conn = DataBaseConnection.getNewConnection();
        String prepQuerry = "SELECT * FROM records WHERE id = ? ";
        PreparedStatement preparedStatement = conn.prepareStatement(prepQuerry);
        preparedStatement.setObject(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        conn.close();
        Record record = new Record();
        if (resultSet.next()) {
            record.setId((UUID)resultSet.getObject("id"));
            record.setDoctor(new DoctorDAO().getDoctorById((UUID)resultSet.getObject("id_doctor")));
            record.setPatient(new PatientDAO().getPatientById((UUID)resultSet.getObject("id_patient")));
            record.setStart(new LocalTime(resultSet.getObject("start_time")));
            record.setEnd(new LocalTime(resultSet.getObject("end_time")));
        }
        logger.info("Get "+record.toString());
        return record;
    }

    @Override
    public void addRecord(Record record) throws ClassNotFoundException, SQLException {
        logger.info("Try to add " + record.toString());
        if(record.getId()==null) {
            Connection conn = DataBaseConnection.getNewConnection();
            String preparedQuerry = "INSERT INTO records (id_doctor,id_patient,start_time,end_time) VALUES(?,?,?,?)";
            PreparedStatement preparedStmt = conn.prepareStatement(preparedQuerry);
            preparedStmt.setObject(1, record.getDoctor().getId());
            preparedStmt.setObject(2, record.getPatient().getId());
            preparedStmt.setTime(3, new Time(record.getStart().toDateTimeToday().getMillis()));
            preparedStmt.setTime(4, new Time(record.getEnd().toDateTimeToday().getMillis()));
            preparedStmt.executeUpdate();
            logger.info("Added "+record.toString());

        }
    }

    @Override
    public void deleteRecordById(UUID id) throws ClassNotFoundException, SQLException {
        logger.info("Try to delete record by id "+id);
        Connection conn = DataBaseConnection.getNewConnection();
        String prepDelete = "DELETE FROM records WHERE id= ?;";
        PreparedStatement preparedStatement = conn.prepareStatement(prepDelete);
        preparedStatement.setObject(1, id);
        int rs=preparedStatement.executeUpdate();
        conn.close();
        if (rs == 0) {
            logger.error("Record "+id+" not found");
            throw new IllegalArgumentException("Record "+id+" not found");
        }
        logger.info("Deleted record: "+id);
    }

    @Override
    public void updateRecordById(Record record) throws ClassNotFoundException, SQLException {
        logger.info("Try to update record by id "+record.toString());
        Connection conn = DataBaseConnection.getNewConnection();
        String prepUpdate = "UPDATE records SET id_doctor=?,id_patient =?,start_time=?,end_time=?" +
                " WHERE id=?;";
        PreparedStatement preparedStatement = conn.prepareStatement(prepUpdate);
        preparedStatement.setObject(1, record.getDoctor().getId());
        preparedStatement.setObject(2, record.getPatient().getId());
        preparedStatement.setTime(3, new Time(record.getStart().toDateTimeToday().getMillis()));
        preparedStatement.setTime(4, new Time(record.getEnd().toDateTimeToday().getMillis()));
        preparedStatement.setObject(5, record.getId());
        int res = preparedStatement.executeUpdate();
        conn.close();
        if (res == 0){
            logger.error(record.toString()+" not found");
            throw new IllegalArgumentException(record.toString()+" not found");
        }
        logger.info("Updated : "+record.toString());
    }

    @Override
    public List<Record> getRecordsByDoctor(Doctor doctor) throws ClassNotFoundException, SQLException {
        logger.info("Try to get records by "+doctor.toString() );
        List<Record> records = new LinkedList<>();
        Connection conn = DataBaseConnection.getNewConnection();
        String prepQuerry = "SELECT * FROM records WHERE id_doctor=? ORDER BY start_time ASC ";
        PreparedStatement preparedStatement = conn.prepareStatement(prepQuerry);
        preparedStatement.setObject(1, doctor.getId());
        ResultSet resultSet= preparedStatement.executeQuery();
        conn.close();
        while (resultSet.next()) {
            Record record=new Record();
            record.setId((UUID) resultSet.getObject("id"));
            record.setDoctor(doctor);
            record.setPatient(new PatientDAO().getPatientById((UUID)resultSet.getObject("id_patient")));
            record.setStart(new LocalTime(resultSet.getObject("start_time")));
            record.setEnd(new LocalTime(resultSet.getObject("end_time")));
            records.add(record);
        }
        logger.info("Records by "+doctor.toString()+"\n" + records.toString());
        return records;
    }

    @Override
    public LinkedList<Record> getRecordsByPatient(Patient patient) throws ClassNotFoundException, SQLException, BadTimeException {
        logger.info("Try to get records by "+patient.toString() );
        LinkedList<Record> records = new LinkedList<>();
        Connection conn = DataBaseConnection.getNewConnection();
        String prepQuerry = "SELECT * FROM records WHERE id_patient=? ORDER BY start_time ASC";
        PreparedStatement preparedStatement = conn.prepareStatement(prepQuerry);
        preparedStatement.setObject(1, patient.getId());
        ResultSet resultSet= preparedStatement.executeQuery();
        conn.close();
        while (resultSet.next()) {
            Record record=new Record();
            record.setId((UUID) resultSet.getObject("id"));
            record.setDoctor(new DoctorDAO().getDoctorByIdWithoutRecords((UUID) resultSet.getObject("id_doctor")));
            record.setPatient(patient);
            record.setStart(new LocalTime(resultSet.getObject("start_time")));
            record.setEnd(new LocalTime(resultSet.getObject("end_time")));
            records.add(record);
        }
        logger.info("Records by "+patient.toString()+"\n" + records.toString());
        return records;
    }

    @Override
    public List<Record> getAllRecords() throws ClassNotFoundException, SQLException, BadTimeException {
        logger.info("Try to get all records " );
        List<Record> records = new ArrayList<>();
        Connection conn = DataBaseConnection.getNewConnection();
        String prepQuerry = "SELECT * FROM records ";
        PreparedStatement preparedStatement = conn.prepareStatement(prepQuerry);
        ResultSet resultSet= preparedStatement.executeQuery();
        conn.close();
        while (resultSet.next()) {
            Record record=new Record();
            record.setId((UUID) resultSet.getObject("id"));
            record.setDoctor(new DoctorDAO().getDoctorById((UUID) resultSet.getObject("id_doctor")));
            record.setPatient(new PatientDAO().getPatientById((UUID)resultSet.getObject("id_patient")));
            record.setStart(new LocalTime(resultSet.getObject("start_time")));
            record.setEnd(new LocalTime(resultSet.getObject("end_time")));
            records.add(record);
        }
        logger.info("All records: " + records.toString());
        return records;
    }
}

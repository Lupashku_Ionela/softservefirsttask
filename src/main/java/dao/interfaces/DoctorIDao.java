package dao.interfaces;

import exception.BadTimeException;
import exception.DeleteException;
import model.Doctor;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public interface DoctorIDao {
    Doctor getDoctorById(UUID id) throws ClassNotFoundException, SQLException, BadTimeException;
    Doctor getDoctorByIdWithoutRecords(UUID id) throws ClassNotFoundException, SQLException, BadTimeException;
    Doctor getDoctorByLogin(String login) throws ClassNotFoundException, SQLException, BadTimeException;
    void addDoctor(Doctor doctor) throws ClassNotFoundException, SQLException;
    void deleteDoctorById(UUID id) throws ClassNotFoundException, SQLException, DeleteException;
    void updateDoctorById(Doctor doctor) throws ClassNotFoundException, SQLException;
    List<Doctor> getAllDoctors() throws ClassNotFoundException, SQLException;
    List<Doctor> getAllActiveDoctors() throws ClassNotFoundException, SQLException, BadTimeException;
    List<Doctor> getAllDisabledDoctors() throws ClassNotFoundException, SQLException, BadTimeException;
}

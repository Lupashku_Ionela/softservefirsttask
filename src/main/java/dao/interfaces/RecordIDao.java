package dao.interfaces;

import exception.BadTimeException;
import model.Doctor;
import model.Patient;
import model.Record;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public interface RecordIDao {
    Record getRecordById(UUID id) throws ClassNotFoundException, SQLException, BadTimeException;
    void addRecord(Record record) throws ClassNotFoundException, SQLException;
    void deleteRecordById(UUID id) throws ClassNotFoundException, SQLException;
    void updateRecordById(Record record) throws ClassNotFoundException, SQLException;
    List<Record> getRecordsByDoctor(Doctor doctor) throws ClassNotFoundException, SQLException;
    LinkedList<Record> getRecordsByPatient(Patient patient) throws ClassNotFoundException, SQLException, BadTimeException;
    List<Record> getAllRecords() throws ClassNotFoundException, SQLException, BadTimeException;
}

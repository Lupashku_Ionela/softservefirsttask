package dao.interfaces;

import exception.DeleteException;
import model.User;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public interface UserIDao {
    User getUserById(UUID id) throws ClassNotFoundException, SQLException;
    User getUserByLogin(String login) throws ClassNotFoundException, SQLException;
    boolean isUserActive(String login) throws SQLException, ClassNotFoundException;
    boolean isUserInDB(String login) throws SQLException, ClassNotFoundException;
    UUID addUser(User user) throws ClassNotFoundException, SQLException;
    void makeUserPassiveById(UUID id) throws ClassNotFoundException, SQLException, DeleteException;
    void makeUserActiveById(UUID id) throws ClassNotFoundException, SQLException, DeleteException;
    void deleteUserById(UUID id) throws ClassNotFoundException, SQLException, DeleteException;
    void updateUserById(User user) throws ClassNotFoundException, SQLException;
    List<User> getAllUsers() throws ClassNotFoundException, SQLException;

}

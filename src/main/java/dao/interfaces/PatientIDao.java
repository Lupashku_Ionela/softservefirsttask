package dao.interfaces;

import exception.DeleteException;
import model.Patient;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public interface PatientIDao {
    Patient getPatientById(UUID id) throws ClassNotFoundException, SQLException;
    Patient getPatientByLogin(String login) throws ClassNotFoundException, SQLException;
    void addPatient(Patient patient) throws ClassNotFoundException, SQLException;
    void deletePatientById(UUID id) throws ClassNotFoundException, SQLException, DeleteException;
    void updatePatientById(Patient patient) throws ClassNotFoundException, SQLException;
    List<Patient> getAllPatients() throws ClassNotFoundException, SQLException;
    List<Patient> getAllActivePatients() throws ClassNotFoundException, SQLException;
    List<Patient> getAllDisabledPatients() throws ClassNotFoundException, SQLException;
}

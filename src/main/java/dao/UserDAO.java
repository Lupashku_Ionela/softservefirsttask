package dao;

import dao.interfaces.UserIDao;
import dataBase.DataBaseConnection;
import exception.DeleteException;
import model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserDAO implements UserIDao {
    private final Logger logger = LogManager.getLogger(UserDAO.class);
    @Override
    public User getUserById(UUID id) throws ClassNotFoundException, SQLException {
        logger.info("Try to get user by id " + id);
        Connection conn = DataBaseConnection.getNewConnection();
        String prepQuerry = "SELECT * FROM users WHERE id = ?";
        PreparedStatement preparedStatement = conn.prepareStatement(prepQuerry);
        preparedStatement.setObject(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();

        User user = null;
        if (resultSet.next()) {
            user = new User();
            user.setId((UUID) resultSet.getObject("id"));
            user.setLogin(resultSet.getString("login"));
            user.setPassword(resultSet.getString("password"));
            user.setRole(Role.valueOf(resultSet.getString("role")));

            if (resultSet.getString("role").equals(String.valueOf("DOCTOR"))) {
                String prepQuerry2 = "SELECT * FROM doctor WHERE id = ?";
                PreparedStatement pr2 = conn.prepareStatement(prepQuerry2);
                pr2.setObject(1, id);
                ResultSet rs2 = pr2.executeQuery();
                conn.close();
                if (rs2.next()) {
                    user.setFirstName(rs2.getString("first_name"));
                    user.setLastName(rs2.getString("last_name"));
                }
            }
            if (resultSet.getString("role").equals(String.valueOf("PATIENT"))) {
                String prepQuerry2 = "SELECT * FROM patient WHERE id = ?";
                PreparedStatement pr2 = conn.prepareStatement(prepQuerry2);
                pr2.setObject(1, id);
                ResultSet rs2 = pr2.executeQuery();
                conn.close();
                if (rs2.next()) {
                    user.setFirstName(rs2.getString("first_name"));
                    user.setLastName(rs2.getString("last_name"));
                }
            }
        }
        logger.info("Get "+user.toString());
        return user;
    }

    @Override
    public User getUserByLogin(String login) throws ClassNotFoundException, SQLException {
        logger.info("Try to get user by login " + login);
        Connection conn = DataBaseConnection.getNewConnection();
        String prepQuerry = "SELECT * FROM users WHERE login = ?";
        PreparedStatement preparedStatement = conn.prepareStatement(prepQuerry);
        preparedStatement.setObject(1, login);
        ResultSet resultSet = preparedStatement.executeQuery();

        User user = null;
        if (resultSet.next()) {
            user = new User();
            user.setId((UUID) resultSet.getObject("id"));
            user.setLogin(resultSet.getString("login"));
            user.setPassword(resultSet.getString("password"));
            user.setRole(Role.valueOf(resultSet.getString("role")));
            logger.info("Try to get by login " + login);
            if (resultSet.getString("role").equals(String.valueOf("DOCTOR"))) {

                String prepQuerry2 = "SELECT * FROM doctor WHERE id = ?";
                PreparedStatement pr2 = conn.prepareStatement(prepQuerry2);
                pr2.setObject(1, user.getId());
                ResultSet rs2 = pr2.executeQuery();
                if (rs2.next()) {
                    user.setFirstName(rs2.getString("first_name"));
                    user.setLastName(rs2.getString("last_name"));
                }
            }
            if (resultSet.getString("role").equals(String.valueOf("PATIENT"))) {
                String prepQuerry3 = "SELECT * FROM patient WHERE id = ?";
                PreparedStatement pr3 = conn.prepareStatement(prepQuerry3);
                pr3.setObject(1, user.getId());
                ResultSet rs3 = pr3.executeQuery();
                if (rs3.next()) {
                    user.setFirstName(rs3.getString("first_name"));
                    user.setLastName(rs3.getString("last_name"));
                    logger.info("Get "+user.toString());
                }
            }
        }
        conn.close();
        return user;
    }

    @Override
    public boolean isUserActive(String login) throws SQLException, ClassNotFoundException {
        logger.info("Search user in DB with login for status: " + login);
        Connection conn = DataBaseConnection.getNewConnection();
        String prepQuerry = "SELECT * FROM users WHERE login = ? AND active=TRUE ";
        PreparedStatement preparedStatement = conn.prepareStatement(prepQuerry);
        preparedStatement.setObject(1, login);
        ResultSet resultSet = preparedStatement.executeQuery();
        conn.close();
        if (resultSet.next()) {
            logger.info("Find user with login " + login);
            return true;
        }
        else {
            logger.info("Didn't find user with login " + login);
            return false;
        }
    }

    @Override
    public boolean isUserInDB(String login) throws SQLException, ClassNotFoundException {
        logger.info("Search user in DB with login: " + login);
        Connection conn = DataBaseConnection.getNewConnection();
        String prepQuerry = "SELECT * FROM users WHERE login = ?";
        PreparedStatement preparedStatement = conn.prepareStatement(prepQuerry);
        preparedStatement.setObject(1, login);
        ResultSet resultSet = preparedStatement.executeQuery();
        conn.close();
        if (resultSet.next()) {
            logger.info("Find user with this login " + login);
            return true;
        }
        else {
            logger.info("Didn't find user with login " + login);
            return false;
        }
    }

    @Override
    public UUID addUser(User user) throws ClassNotFoundException, SQLException {
        logger.info("Try to add " + user.toString());
        if (user.getId() == null) {
            Connection conn = DataBaseConnection.getNewConnection();
            Statement st = conn.createStatement();
            String preparedQuerry = "INSERT INTO users (login,password,role) VALUES(?,?,?)";
            PreparedStatement preparedStmt = conn.prepareStatement(preparedQuerry);
            preparedStmt.setString(1, user.getLogin());
            preparedStmt.setString(2, user.getPassword());
            preparedStmt.setString(3, user.getRole().toString());
            preparedStmt.executeUpdate();

            String preparedQuery1 = "SELECT id FROM users WHERE login=?;";
            PreparedStatement preparedStatement = conn.prepareStatement(preparedQuery1);
            preparedStatement.setString(1, user.getLogin());
            ResultSet resultSet = preparedStatement.executeQuery();
            logger.info("Added "+user.toString());
            UUID uuid=null;
            if (resultSet.next()) {
                uuid= ((UUID) resultSet.getObject("id"));
            }
            return uuid;
        }
        else return user.getId();
    }

    @Override
    public void makeUserPassiveById(UUID id) throws ClassNotFoundException, SQLException, DeleteException {
        logger.info("Try to make user passive " + id);
        Connection conn = DataBaseConnection.getNewConnection();
        String prepUpdate = "UPDATE users SET active=FALSE " +
                " WHERE id=?;";

        PreparedStatement preparedStatement = conn.prepareStatement(prepUpdate);
        preparedStatement.setObject(1, id);
        conn.close();
    }

    @Override
    public void makeUserActiveById(UUID id) throws ClassNotFoundException, SQLException, DeleteException {
        logger.info("Try to make user active " + id);
        Connection conn = DataBaseConnection.getNewConnection();
        String prepUpdate = "UPDATE users SET active=TRUE " +
                " WHERE id=?;";

        PreparedStatement preparedStatement = conn.prepareStatement(prepUpdate);
        preparedStatement.setObject(1, id);
        conn.close();
    }

    @Override
    public void deleteUserById(UUID id) throws ClassNotFoundException, SQLException, DeleteException {
        logger.info("Try to delete user by id "+id);
        Connection conn = DataBaseConnection.getNewConnection();
        String prepDelete = "DELETE FROM users WHERE id= ?;";
        PreparedStatement preparedStatement = conn.prepareStatement(prepDelete);
        preparedStatement.setObject(1, id);
        int rs=preparedStatement.executeUpdate();
        conn.close();
        if (rs == 0) {
            logger.error("Doctor "+id+" not found");
            throw new IllegalArgumentException("Doctor "+id+" not found");
        }
        logger.info("User "+id+" deleted");
    }

    @Override
    public void updateUserById(User user) throws ClassNotFoundException, SQLException {
        logger.info("Try to update user by id "+user.toString());
        Connection conn = DataBaseConnection.getNewConnection();
        String prepUpdate = "UPDATE users SET login=?,password =?, role =?" +
                " WHERE id=?;";

        PreparedStatement preparedStatement = conn.prepareStatement(prepUpdate);
        preparedStatement.setString(1, user.getLogin());
        preparedStatement.setString(2, user.getPassword());
        preparedStatement.setString(3, user.getRole().toString());
        preparedStatement.setObject(4, user.getId());
        int res = preparedStatement.executeUpdate();
        conn.close();
        if (res == 0){
            logger.error(user.toString()+" not found");
            throw new IllegalArgumentException(user.toString()+" not found");
        }
        logger.info("Updated : "+user.toString());
    }

    @Override
    public List<User> getAllUsers() throws ClassNotFoundException, SQLException {
        logger.info("Try to get all doctors " );
        List<User> users = new ArrayList<>();
        Connection conn = DataBaseConnection.getNewConnection();
        String prepQuerry = "SELECT * FROM users ";
        PreparedStatement preparedStatement = conn.prepareStatement(prepQuerry);
        ResultSet resultSet = preparedStatement.executeQuery();
        conn.close();
        while (resultSet.next()) {
            User user = new User();
            user.setId((UUID) resultSet.getObject("id"));
            user.setLogin(resultSet.getString("login"));
            user.setPassword(resultSet.getString("password"));
            user.setRole(Role.valueOf(resultSet.getString("role")));
            users.add(user);
        }
        logger.info("All users: " + users.toString());
        return users;
    }
}

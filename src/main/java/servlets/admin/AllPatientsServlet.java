package servlets.admin;
import model.Patient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.AdminService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "patients", urlPatterns = {"/patients"})
public class AllPatientsServlet extends HttpServlet{

    private AdminService adminService=new AdminService();
    private final Logger logger = LogManager.getLogger(AllPatientsServlet.class);
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            logger.info("All patients servlet");
            List<Patient>  activePatients= adminService.getAllActivePatients();
            List<Patient> disabledPatients=adminService.getAllDisabledPatients();

            request.setAttribute("activePatients",activePatients);
            request.setAttribute("disabledPatients",disabledPatients);

        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
        getServletContext().getRequestDispatcher("/views/admin/AllPatients.jsp").forward(request, response);
    }
}
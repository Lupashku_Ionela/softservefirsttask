package servlets.admin;

import exception.BadTimeException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.LocalTime;
import service.DoctorService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "newDoctor", urlPatterns = {"/newDoctor"})

public class AddDoctorServlet extends HttpServlet {
    private final Logger logger = LogManager.getLogger(AddDoctorServlet.class);
    DoctorService doctorService=new DoctorService();
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/views/admin/NewDoctor.jsp").forward(request, response);
    }
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Add Doctor servlet");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String login=request.getParameter("login");
        String password=request.getParameter("password");
        String confirmPassword=request.getParameter("confirm_password");
        LocalTime start_time=new LocalTime(Integer.parseInt(request.getParameter("start_time_hours")),
                Integer.parseInt(request.getParameter("start_time_minutes")));
        LocalTime end_time=new LocalTime(Integer.parseInt(request.getParameter("end_time_hours")),
                Integer.parseInt(request.getParameter("end_time_minutes")));
        List<String> errors;
        try {
            errors = doctorService.registerDoctor(firstName,lastName,start_time,end_time,login,password,confirmPassword);
            if(!errors.isEmpty()) {
                logger.error("Register error"+errors);
                request.setAttribute("registerError", errors);
                request.setAttribute("firstName",firstName);
                request.setAttribute("lastName",lastName);
                request.setAttribute("login",login);
                getServletContext().getRequestDispatcher("/views/admin/NewDoctor.jsp").forward(request, response);
            }
            else {
                logger.info("Successful added doctor");
                errors.add("Successful added doctor");
                request.setAttribute("success", errors);
                getServletContext().getRequestDispatcher("/views/admin/NewDoctor.jsp").forward(request, response);
            }
        } catch (SQLException | ClassNotFoundException | BadTimeException e) {
            logger.error(e.getMessage());
        }

    }
}


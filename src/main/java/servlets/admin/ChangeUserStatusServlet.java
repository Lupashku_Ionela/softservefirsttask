package servlets.admin;

import exception.DeleteException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.UUID;

@WebServlet(name = "changeStatus", urlPatterns = {"/changeStatus"})

public class ChangeUserStatusServlet extends HttpServlet {
    private final Logger logger = LogManager.getLogger(ChangeUserStatusServlet.class);
    private UserService userService = new UserService();

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UUID id = UUID.fromString(request.getParameter("idUser"));
        boolean status = Integer.parseInt(request.getParameter("status")) == 1;

        try {
            userService.changeUserStatus(id, status);
            logger.info("Successful change status");
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath()+"/"));
        } catch (SQLException | ClassNotFoundException | DeleteException e) {
            logger.error(e.getMessage());
        }

    }
}


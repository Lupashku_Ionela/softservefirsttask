package servlets.admin;
import exception.BadTimeException;
import model.Doctor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.AdminService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "doctors", urlPatterns = {"/doctors"})
public class AllDoctorsServlet extends HttpServlet{
    private final Logger logger = LogManager.getLogger(AllDoctorsServlet.class);
    private AdminService adminService=new AdminService();
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            logger.info("All doctors servlet");
            List<Doctor>  activeDoctors= adminService.getAllActiveDoctors();
            List<Doctor> disabledDoctors=adminService.getAllDisabledDoctors();

            request.setAttribute("activeDoctors",activeDoctors);
            request.setAttribute("disabledDoctors",disabledDoctors);

        } catch (SQLException | ClassNotFoundException | BadTimeException e) {
            logger.error(e.getMessage());
        }
        getServletContext().getRequestDispatcher("/views/admin/AllDoctors.jsp").forward(request, response);
    }
}
package servlets;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "signIn", urlPatterns = {"/signIn"})
public class SignInServlet extends HttpServlet {
    private final Logger logger = LogManager.getLogger(SignInServlet.class);
        public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            getServletContext().getRequestDispatcher("/views/Enter.jsp").forward(request, response);
        }
        public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            UserService userService = new UserService();
            String login=request.getParameter("login");
            String password=request.getParameter("password");
            String loginError;

                try {
                    loginError = userService.loginUserByPassword(login, password);
                    if (loginError != null) {
                        request.setAttribute("loginError", loginError);
                        request.setAttribute("login",login);
                        request.getRequestDispatcher("/views/Enter.jsp").forward(request, response);
                    } else {
                        HttpSession session = request.getSession();
                        session.setAttribute("currUser", new UserService().getUserByLogin(login));
                        session.setMaxInactiveInterval(800);
                        response.sendRedirect(response.encodeRedirectURL(request.getContextPath()+"/"));
                    }
                } catch (SQLException | ClassNotFoundException e) {
                    logger.error(e.getMessage());
                }
            }
        }

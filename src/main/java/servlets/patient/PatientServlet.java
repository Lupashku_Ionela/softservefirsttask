package servlets.patient;
import model.Patient;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.PatientService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "patient", urlPatterns = {"/patient"})
public class PatientServlet extends HttpServlet{

    private PatientService patientService=new PatientService();
    private final Logger logger = LogManager.getLogger(PatientService.class);
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            User user=(User) request.getSession().getAttribute("currUser");
            Patient patient = patientService.getPatientById(user.getId());
            request.setAttribute("patient",patient);

        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e.getMessage());
        }
        getServletContext().getRequestDispatcher("/views/patient/Page.jsp").forward(request, response);
    }
}

package servlets.patient;

import exception.BadTimeException;
import model.Doctor;
import model.Patient;
import model.Record;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.DoctorService;
import service.PatientService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@WebServlet(name = "addRecordDoctor",urlPatterns = {"/addRecordDoctor"})
public class AddRecordDoctorServlet extends HttpServlet {
    private final Logger logger = LogManager.getLogger(AddRecordDoctorServlet.class);

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            logger.info("Try to open add record page ");
            User user=(User) request.getSession().getAttribute("currUser");
            UUID id = UUID.fromString(request.getParameter("idDoctor"));
            Patient patient = new PatientService().getPatientById(user.getId());
            Doctor doctor=new DoctorService().getDoctorById(id);
            List<Record> ranges=new DoctorService().freeRanges(id);
            request.setAttribute("ranges",ranges);
            request.setAttribute("doctor",doctor);
            request.setAttribute("patient", patient);

        } catch (SQLException | ClassNotFoundException |BadTimeException e) {
           logger.error(e.getMessage());
        }
        getServletContext().getRequestDispatcher("/views/patient/AddRecordToDoctor.jsp").forward(request, response);
    }
}

package servlets.patient;
import exception.BadTimeException;
import exception.DeleteException;
import model.Patient;
import model.Record;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.PatientService;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@WebServlet(name = "appointments", urlPatterns = {"/appointments"})
public class AppointmentsServlet extends HttpServlet{

    private PatientService patientService=new PatientService();
    private final Logger logger = LogManager.getLogger(AppointmentsServlet.class);
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            User user=(User) request.getSession().getAttribute("currUser");
            Patient patient = patientService.getPatientById(user.getId());
            request.setAttribute("patient",patient);
            List<Record> records=patientService.getAllRecords(patient);
            request.setAttribute("records",records);

        } catch (SQLException | ClassNotFoundException | BadTimeException e) {
            logger.error(e.getMessage());
        }
        getServletContext().getRequestDispatcher("/views/patient/Appointments.jsp").forward(request, response);
    }
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Try to revoke record");
        UUID id = UUID.fromString(request.getParameter("idRecord"));
        List<String> errors=null;
        try {
            User user=(User) request.getSession().getAttribute("currUser");
            logger.info("Try to delete record : "+id);
            errors = new UserService().revokeRecord(id);
            Patient patient = patientService.getPatientById(user.getId());
            request.setAttribute("patient",patient);
            List<Record> records=patientService.getAllRecords(patient);
            request.setAttribute("records",records);
            if(!errors.isEmpty()) {
                logger.error("Revoke record error"+errors);
                request.setAttribute("errors", errors);
                getServletContext().getRequestDispatcher("/views/patient/Appointments.jsp").forward(request, response);
                return;
            }
            logger.info("Successful delete");
            errors.add("Successful revoke appointment");
            request.setAttribute("success", errors);
            getServletContext().getRequestDispatcher("/views/patient/Appointments.jsp").forward(request, response);

        } catch (SQLException | ClassNotFoundException | DeleteException | BadTimeException e) {
            logger.error(e.getMessage());
        }
    }
}
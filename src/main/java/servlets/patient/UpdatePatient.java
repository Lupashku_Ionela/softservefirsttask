package servlets.patient;

import exception.BadTimeException;
import model.Patient;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.LocalDate;
import service.PatientService;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "updatePatient",urlPatterns = {"/updatePatient"})
public class UpdatePatient extends HttpServlet {
    private final Logger logger = LogManager.getLogger(UpdatePatient.class);
    private PatientService patientService=new PatientService();

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String login=request.getParameter("login");
        LocalDate date= LocalDate.parse(request.getParameter("dateB"));
        String phone=request.getParameter("phone");
        List<String> errors;
        try {
            User user=(User) request.getSession().getAttribute("currUser");
            Patient patient = patientService.getPatientById(user.getId());
            errors = patientService.updatePatientInfo(patient,firstName,lastName,login, phone,date);
            if(!errors.isEmpty()) {
                logger.error("Update error"+errors);
                request.setAttribute("error", errors);
                request.setAttribute("patient",patient);
                getServletContext().getRequestDispatcher("/views/patient/Page.jsp").forward(request, response);
                return;
            }
                logger.info("Successful update patient");
               request.getSession().setAttribute("currUser",new UserService().getUserById(user.getId()));
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath()+"/"));
            logger.info("Successful redirect");

        } catch (SQLException | ClassNotFoundException | BadTimeException e) {
            logger.error(e.getMessage());
        }
    }
}

package servlets.patient;

import exception.DeleteException;
import model.Patient;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.PatientService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "deletePatient",urlPatterns = {"/deletePatient"})
public class DeletePatientServlet extends HttpServlet {
    private final Logger logger = LogManager.getLogger(DeletePatientServlet.class);
    private PatientService patientService=new PatientService();

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Try to delete patient");
        String currentPass = request.getParameter("currentPass");
        List<String> errors=null;
        try {
            User user=(User) request.getSession().getAttribute("currUser");
            Patient patient = patientService.getPatientById(user.getId());
            logger.info("Try to delete patient : "+patient.toString());
            errors = patientService.deletePatient(patient,currentPass);
            if(!errors.isEmpty()) {
                logger.error("Change password error"+errors);
                request.setAttribute("errors", errors);
                request.setAttribute("patient",patient);
                getServletContext().getRequestDispatcher("/views/patient/Page.jsp").forward(request, response);
                return;
            }
            logger.info("Successful delete");

            request.getSession().invalidate();
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath()+"/"));

        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e.getMessage());
        } catch (DeleteException e) {
            request.setAttribute("delException", e.getMessage());
        }
    }

}

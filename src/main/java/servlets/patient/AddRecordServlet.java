package servlets.patient;

import exception.BadTimeException;
import model.Doctor;
import model.Patient;
import model.Record;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.LocalTime;
import service.DoctorService;
import service.PatientService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@WebServlet(name = "record", urlPatterns = {"/record"})
public class AddRecordServlet extends HttpServlet {
    private final Logger logger = LogManager.getLogger(AddRecordServlet.class);
    private PatientService patientService = new PatientService();

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Try to add record");
        UUID id = UUID.fromString(request.getParameter("idDoctor"));
        LocalTime time = LocalTime.parse(request.getParameter("start_time"));
        int duration = Integer.parseInt(request.getParameter("duration"));
        List<String> errors;
        try {
            User user = (User) request.getSession().getAttribute("currUser");
            Patient patient = patientService.getPatientById(user.getId());
            logger.info("Try to add record for : " + patient.toString());
            errors = patientService.addRecord(patient, id, time, duration);
            Doctor doctor=new DoctorService().getDoctorById(id);
            request.setAttribute("doctor",doctor);
            List<Record> ranges=new DoctorService().freeRanges(id);
            request.setAttribute("ranges",ranges);
            request.setAttribute("patient", patient);
            if (!errors.isEmpty()) {
                logger.error("Add record error" + errors);
                request.setAttribute("errors", errors);
                request.setAttribute("duration",duration);
                request.setAttribute("time",time);

                List<LocalTime> times=patientService.freeTimesForDuration(id,time,duration);
                request.setAttribute("times", times);
                if(!times.isEmpty()) request.setAttribute("nearestTime", times.get(0));
                getServletContext().getRequestDispatcher("/views/patient/AddRecordToDoctor.jsp").forward(request, response);
                return;
            }
            logger.info("Successful add record");
            errors.add("Successful add record");
            request.setAttribute("success", errors);
            getServletContext().getRequestDispatcher("/views/patient/AddRecordToDoctor.jsp").forward(request, response);
        } catch (SQLException | ClassNotFoundException | BadTimeException e) {
            logger.error(e.getMessage());
        }
    }
}

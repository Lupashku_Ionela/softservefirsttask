package servlets.patient;
import exception.BadTimeException;
import model.Doctor;
import model.Patient;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.PatientService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "allDoctors", urlPatterns = {"/allDoctors"})
public class ShowAllDoctorsServlet extends HttpServlet{

    private PatientService patientService=new PatientService();
    private final Logger logger = LogManager.getLogger(ShowAllDoctorsServlet.class);
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            User user=(User) request.getSession().getAttribute("currUser");
            Patient patient = patientService.getPatientById(user.getId());
            List<Doctor> doctorList=patientService.getAllActiveDoctors() ;
            request.setAttribute("patient",patient);
            request.setAttribute("doctors",doctorList);

        } catch (SQLException | ClassNotFoundException | BadTimeException e) {
            logger.error(e.getMessage());
        }
        getServletContext().getRequestDispatcher("/views/patient/Doctors.jsp").forward(request, response);
    }
}
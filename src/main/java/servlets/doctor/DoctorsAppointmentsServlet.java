package servlets.doctor;
import exception.BadTimeException;
import exception.DeleteException;
import model.Doctor;
import model.Record;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.DoctorService;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@WebServlet(name = "doctorsAppointments", urlPatterns = {"/doctorsAppointments"})
public class DoctorsAppointmentsServlet extends HttpServlet{

    private DoctorService doctorService=new DoctorService();
    private final Logger logger = LogManager.getLogger(DoctorsAppointmentsServlet.class);
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            User user=(User) request.getSession().getAttribute("currUser");
            Doctor doctor = doctorService.getDoctorById(user.getId());
            request.setAttribute("doctor",doctor);
            List<Record> records=doctorService.getAllRecords(doctor);
            request.setAttribute("records",records);

        } catch (SQLException | ClassNotFoundException | BadTimeException e) {
            logger.error(e.getMessage());
        }
        getServletContext().getRequestDispatcher("/views/doctor/Appointments.jsp").forward(request, response);
    }
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Try to revoke record");
        UUID id = UUID.fromString(request.getParameter("idRecord"));
        List<String> errors=null;
        try {
            User user=(User) request.getSession().getAttribute("currUser");
            logger.info("Try to delete record : "+id);
            errors = new UserService().revokeRecord(id);
            Doctor doctor = doctorService.getDoctorById(user.getId());
            request.setAttribute("doctor",doctor);
            List<Record> records=doctorService.getAllRecords(doctor);
            request.setAttribute("records",records);
            if(!errors.isEmpty()) {
                logger.error("Revoke record error"+errors);
                request.setAttribute("errors", errors);
                getServletContext().getRequestDispatcher("/views/doctor/Appointments.jsp").forward(request, response);
                return;
            }
            logger.info("Successful delete");
            errors.add("Successful revoke appointment");
            request.setAttribute("success", errors);
            getServletContext().getRequestDispatcher("/views/doctor/Appointments.jsp").forward(request, response);

        } catch (SQLException | ClassNotFoundException | DeleteException | BadTimeException e) {
            logger.error(e.getMessage());
        }
    }
    }

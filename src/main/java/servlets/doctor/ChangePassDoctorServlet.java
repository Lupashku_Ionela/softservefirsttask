package servlets.doctor;

import exception.BadTimeException;
import model.Doctor;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.DoctorService;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "changePassDoctor",urlPatterns = {"/changePassDoctor"})
public class ChangePassDoctorServlet extends HttpServlet {
    private final Logger logger = LogManager.getLogger(ChangePassDoctorServlet.class);
    private DoctorService doctorService=new DoctorService();

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Try to change password");
        String newPass = request.getParameter("newPass");
        String confirmNew = request.getParameter("newConfPass");
        String currentPass = request.getParameter("currentPass");

        List<String> errors;
        try {
            User user=(User) request.getSession().getAttribute("currUser");
            Doctor doctor = doctorService.getDoctorById(user.getId());
            logger.info("Try to change password for : "+doctor.toString());
            errors = doctorService.updateDoctorPassword(doctor,newPass,confirmNew,currentPass);
            if(!errors.isEmpty()) {
                logger.error("password error"+errors);
                request.setAttribute("errors", errors);
                request.setAttribute("doctor",doctor);
                getServletContext().getRequestDispatcher("/views/doctor/Page.jsp").forward(request, response);
                return;
            }
            logger.info("Successful change password");

            request.getSession().setAttribute("currUser", new UserService().getUserById(user.getId()));
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath()+"/"));
        } catch (SQLException | ClassNotFoundException | BadTimeException e) {
            logger.error(e.getMessage());
        }
    }
}

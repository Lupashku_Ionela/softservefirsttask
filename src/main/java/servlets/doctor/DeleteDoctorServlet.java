package servlets.doctor;

import exception.BadTimeException;
import exception.DeleteException;
import model.Doctor;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.DoctorService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "deleteDoctor",urlPatterns = {"/deleteDoctor"})
public class DeleteDoctorServlet extends HttpServlet {
    private final Logger logger = LogManager.getLogger(DeleteDoctorServlet.class);
    private DoctorService doctorService=new DoctorService();

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Try to delete doctor");
        String currentPass = request.getParameter("currentPass");
        List<String> errors=null;
        try {
            User user=(User) request.getSession().getAttribute("currUser");
            Doctor doctor = doctorService.getDoctorById(user.getId());
            logger.info("Try to delete patient : "+doctor.toString());
            errors = doctorService.deleteDoctor(doctor,currentPass);
            if(!errors.isEmpty()) {
                logger.error("Change password error"+errors);
                request.setAttribute("errors", errors);
                request.setAttribute("doctor",doctor);
                getServletContext().getRequestDispatcher("/views/doctor/Page.jsp").forward(request, response);
                return;
            }
            logger.info("Successful delete");

            request.getSession().invalidate();
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath()+"/"));

        } catch (SQLException | ClassNotFoundException | BadTimeException e) {
            logger.error(e.getMessage());
        } catch (DeleteException e) {
            request.setAttribute("delException", e.getMessage());
        }
    }

}

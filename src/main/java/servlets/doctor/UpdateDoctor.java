package servlets.doctor;

import exception.BadTimeException;
import model.Doctor;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.LocalTime;
import service.DoctorService;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "updateDoctor",urlPatterns = {"/updateDoctor"})
public class UpdateDoctor extends HttpServlet {
    private final Logger logger = LogManager.getLogger(UpdateDoctor.class);
    private DoctorService doctorService=new DoctorService();

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String login=request.getParameter("login");

        LocalTime start_time=new LocalTime(Integer.parseInt(request.getParameter("start_time_hours")),
                Integer.parseInt(request.getParameter("start_time_minutes")));
        LocalTime end_time=new LocalTime(Integer.parseInt(request.getParameter("end_time_hours")),
                Integer.parseInt(request.getParameter("end_time_minutes")));
        List<String> errors;
        try {
            User user=(User) request.getSession().getAttribute("currUser");
            Doctor doctor = doctorService.getDoctorById(user.getId());
            errors = doctorService.updateDoctorInfo(doctor,firstName,lastName,login, start_time,end_time);
            if(!errors.isEmpty()) {
                logger.error("Update error"+errors);
                request.setAttribute("error", errors);
                request.setAttribute("doctor",doctor);
                getServletContext().getRequestDispatcher("/views/doctor/Page.jsp").forward(request, response);
                return;
            }
            logger.info("Successful update doctor");
            request.getSession().setAttribute("currUser",new UserService().getUserById(user.getId()));
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath()+"/"));
            logger.info("Successful redirect");

        } catch (SQLException | ClassNotFoundException | BadTimeException e) {
            logger.error(e.getMessage());
        }
    }
}


package servlets.doctor;
import exception.BadTimeException;
import model.Doctor;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import service.DoctorService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "doctor", urlPatterns = {"/doctor"})
public class DoctorServlet extends HttpServlet{

    private DoctorService doctorService=new DoctorService();
    private final Logger logger = LogManager.getLogger(DoctorServlet.class);
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            User user=(User) request.getSession().getAttribute("currUser");
            Doctor doctor = doctorService.getDoctorById(user.getId());
            request.setAttribute("doctor",doctor);

        } catch (SQLException | ClassNotFoundException | BadTimeException e) {
            logger.error(e.getMessage());
        }
        getServletContext().getRequestDispatcher("/views/doctor/Page.jsp").forward(request, response);
    }

}

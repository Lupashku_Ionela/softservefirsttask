package servlets;

import exception.BadTimeException;
import model.Patient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.LocalDate;
import service.PatientService;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "registration", urlPatterns = {"/registration"})

public class RegistrationServlet extends HttpServlet {
    private final Logger logger = LogManager.getLogger(RegistrationServlet.class);
    PatientService patientService=new PatientService();
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/views/NewUser.jsp").forward(request, response);
    }
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String firstName = request.getParameter("first_name");
        String lastName = request.getParameter("last_name");
        String login=request.getParameter("login");
        String password=request.getParameter("password");
        String confirmPassword=request.getParameter("confirm_password");
        LocalDate date= LocalDate.parse(request.getParameter("dateB"));
        String phone=request.getParameter("phone");
        List<String> errors;
           try {
               errors = new PatientService().registerPatient(firstName,lastName,date,phone,login,password,confirmPassword);
               if(!errors.isEmpty()) {
                   logger.error("Register error"+errors);
                   request.setAttribute("registerError", errors);
                   request.setAttribute("firstName",firstName);
                   request.setAttribute("lastName",lastName);
                   request.setAttribute("login",login);
                   request.setAttribute("dataB",date);
                   request.setAttribute("phone",phone);
                   getServletContext().getRequestDispatcher("/views/NewUser.jsp").forward(request, response);
                   return;
               }
               else {
                   logger.info("Successful added doctor");
                   HttpSession session = request.getSession();
                   session.setAttribute("currUser", new UserService().getUserByLogin(login));
                   Patient patient = patientService.getPatientByLogin(login);
                   request.setAttribute("patient",patient);
                   session.setMaxInactiveInterval(800);
                   response.sendRedirect(response.encodeRedirectURL(request.getContextPath()+"/"));
               }
           } catch (SQLException | ClassNotFoundException | BadTimeException e) {
                logger.error(e.getMessage());
           }

    }
}

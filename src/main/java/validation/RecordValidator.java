package validation;
import org.joda.time.LocalTime;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RecordValidator implements ConstraintValidator<Record, model.Record> {
    public void initialize(Record constraintAnnotation) {
    }

    @Override
    public boolean isValid(model.Record record,ConstraintValidatorContext constraintValidatorContext)  {
        LocalTime startTime = record.getStart();
        LocalTime endTime = record.getEnd();
        if (startTime.isBefore(endTime)&&startTime.isAfter(LocalTime.now())) {
            return true;
        }
        return false;
    }
}


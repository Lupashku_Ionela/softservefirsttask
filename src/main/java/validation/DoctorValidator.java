package validation;
import org.joda.time.LocalTime;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DoctorValidator implements ConstraintValidator<Doctor, model.Doctor> {
    public void initialize(Doctor constraintAnnotation) {
    }

    @Override
    public boolean isValid(model.Doctor doctor,ConstraintValidatorContext constraintValidatorContext)  {
        LocalTime startTime = doctor.getStartTime();
        LocalTime endTime = doctor.getEndTime();
        if (startTime.isBefore(endTime)) {
            return true;
        }
        return false;
    }
}


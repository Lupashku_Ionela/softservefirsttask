package exception;

public class BadTimeException extends Exception{
        public BadTimeException(String msg) {
            super(msg);
        }
}

package dataBase;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.Properties;

public class DataBaseConnection {
    private static final Logger logger = LogManager.getLogger(DataBaseConnection.class);
    private Connection connection;

    public void connectDatabase() {

        Properties props = PropertyManager.getProperties();
        String url = props.getProperty("jdbc.url");

        try {
            connection = DriverManager.getConnection(url, props);

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    public static Connection getNewConnection() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        Properties properties = PropertyManager.getProperties();
        String url = properties.getProperty("jdbc.url");
        return DriverManager.getConnection(url, properties);
    }
    private void disconnectDatabase() {
        if (connection != null)
            try {
                connection.close();
            } catch (SQLException e) {
                logger.error(e.getMessage());
            }
    }
    public void close() {
        disconnectDatabase();
    }
}

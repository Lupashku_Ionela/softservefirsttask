package dataBase;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.util.Properties;

public class PropertyManager {

    private static final String DEFAULT_PROPERTIES_FILE = PropertyManager.class.getClassLoader().getResource("dataBase.properties").getPath();
    private static final Logger logger = LogManager.getLogger(PropertyManager.class);

    private PropertyManager() {
    }

    private static void loadProperties(Properties props) {
        try {
            FileInputStream in = new FileInputStream(DEFAULT_PROPERTIES_FILE);
            props.load(in);
            in.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    public static Properties getProperties() {

        Properties props = new Properties();
        loadProperties(props);
        return props;
    }
}

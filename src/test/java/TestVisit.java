import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import model.Doctor;
import model.Record;
import service.Visit;
import exception.BadTimeException;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class TestVisit {
    private final DateTimeFormatter formatter= DateTimeFormat.forPattern("HH:mm");
    ///////////////////////////////
    @DataProvider
    public Object[][] recordsProvider() {
        Record record1=new Record(formatter.parseLocalTime("9:30"),15);
        Record record2=new Record(formatter.parseLocalTime("10:50"),17);
        Record record3=new Record(formatter.parseLocalTime("8:00"),4);
        Record record4=new Record(formatter.parseLocalTime("17:50"),10);

        List<Record> records1=new LinkedList<>();
        records1.add(new Record(formatter.parseLocalTime("9:00"),30));
        records1.add(new Record(formatter.parseLocalTime("9:45"),17));
        records1.add(new Record(formatter.parseLocalTime("10:05"),45));
        records1.add(new Record(formatter.parseLocalTime("16:00"),20));
        records1.add(new Record(formatter.parseLocalTime("16:45"),15));
        Doctor doctor1=new Doctor();
        Doctor doctor2=new Doctor();
        Doctor doctor3=new Doctor();
        Doctor doctor4=new Doctor();
        doctor1.setStartTime(formatter.parseLocalTime("8:00"));
        doctor1.setEndTime(formatter.parseLocalTime("18:00"));
        doctor2.setStartTime(formatter.parseLocalTime("8:00"));
        doctor2.setEndTime(formatter.parseLocalTime("18:00"));
        doctor3.setStartTime(formatter.parseLocalTime("8:00"));
        doctor3.setEndTime(formatter.parseLocalTime("18:00"));
        doctor4.setStartTime(formatter.parseLocalTime("8:00"));
        doctor4.setEndTime(formatter.parseLocalTime("18:00"));
        doctor1.setRecords(records1);
        doctor2.setRecords(records1);
        doctor3.setRecords(records1);
        doctor4.setRecords(records1);

        return new Object[][] { { doctor1,record1,true },
                { doctor2,record2,true },
                { doctor3,record3,true },
                { doctor4,record4,true }};
    }
    @DataProvider
    public Object[][] negativeRecordsProvider() {
        Record record1=new Record(formatter.parseLocalTime("7:50"),10);
        Record record2=new Record(formatter.parseLocalTime("18:00"),13);
        Record record3=new Record(formatter.parseLocalTime("17:50"),20);

        List<Record> records1=new LinkedList<>();
        records1.add(new Record(formatter.parseLocalTime("9:00"),30));
        records1.add(new Record(formatter.parseLocalTime("9:45"),17));
        records1.add(new Record(formatter.parseLocalTime("10:05"),45));
        records1.add(new Record(formatter.parseLocalTime("16:00"),20));
        records1.add(new Record(formatter.parseLocalTime("16:45"),15));
        Doctor doctor1=new Doctor();
        doctor1.setStartTime(formatter.parseLocalTime("8:00"));
        doctor1.setEndTime(formatter.parseLocalTime("18:00"));
        doctor1.setRecords(records1);


        return new Object[][] { {doctor1,record1},
                {doctor1,record2},
                {doctor1,record3}};
    }
    @Test(dataProvider = "recordsProvider")
    public void testAddRecord(Doctor doctor, Record record, boolean res) throws BadTimeException {
        Visit visit=new Visit();
        assertEquals(res,visit.addRecord(doctor,record));
    }
    @Test(expectedExceptions = BadTimeException.class,dataProvider ="negativeRecordsProvider" )
    public void negativeTestAddRecord(Doctor doctor, Record record) throws BadTimeException {
        Visit visit=new Visit();
        visit.addRecord(doctor,record);
    }
    //////////////////////////////////////////////////////////////

    @DataProvider
    public Object[][] differenceProvider() {
        return new Object[][]{{formatter.parseLocalTime("9:00"),formatter.parseLocalTime("10:00"),60},
                {formatter.parseLocalTime("9:00"),formatter.parseLocalTime("9:01"),1},
                {formatter.parseLocalTime("6:50"),formatter.parseLocalTime("7:12"),22},
                {formatter.parseLocalTime("9:00"),formatter.parseLocalTime("9:00"),0},
                {formatter.parseLocalTime("9:03"),formatter.parseLocalTime("9:08"),5},
                {formatter.parseLocalTime("9:00"),formatter.parseLocalTime("8:00"),60}};
    }
    @Test(dataProvider = "differenceProvider")
    public void testDifferenceTimes(LocalTime first, LocalTime second, int interval){
        Visit visit=new Visit();
        assertEquals(interval,visit.differenceTimes(first,second));
    }
    ///////////////////////////////////////////////////////////////

    @DataProvider
    public Object[][] freeTimesProvider(){
        Record record=new Record(formatter.parseLocalTime("9:05"),15);

        List<Record> records1=new LinkedList<>();
        records1.add(new Record(formatter.parseLocalTime("9:00"),30));
        records1.add(new Record(formatter.parseLocalTime("9:45"),17));
        records1.add(new Record(formatter.parseLocalTime("10:05"),45));
        records1.add(new Record(formatter.parseLocalTime("16:00"),20));
        records1.add(new Record(formatter.parseLocalTime("16:45"),15));
        Doctor doctor1=new Doctor();
        doctor1.setStartTime(formatter.parseLocalTime("8:00"));
        doctor1.setEndTime(formatter.parseLocalTime("18:00"));
        doctor1.setRecords(records1);

        List<LocalTime> res1=new ArrayList<>();
        res1.add(formatter.parseLocalTime("9:30"));
        res1.add(formatter.parseLocalTime("8:00"));
        res1.add(formatter.parseLocalTime("10:50"));
        res1.add(formatter.parseLocalTime("16:20"));
        res1.add(formatter.parseLocalTime("17:00"));
        return  new Object[][]{{doctor1,record,res1}};
    }
    @Test(dataProvider = "freeTimesProvider")
    public void testGetFreeTimes(Doctor doctor,Record record, List<LocalTime> res){
        Visit visit = new Visit();
        assertEquals(res,visit.getFreeTimes(doctor,record));

    }
    ///////////////////////////////////////////////////////////////

    @Test(dataProvider = "freeTimesProvider")
    public void testNearestTime(Doctor doctor,Record record,List<LocalTime> res){
        Visit visit = new Visit();
        assertEquals(res.get(0),visit.nearestTime(doctor,record));
    }
}

--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5
-- Dumped by pg_dump version 11.0

-- Started on 2018-11-21 19:55:11

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2826 (class 0 OID 16838)
-- Dependencies: 197
-- Data for Name: doctor; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.doctor VALUES ('fc672633-056a-4922-a453-bbd757f3015c', 'Nill', 'Toms', '09:00:00', '15:00:00');
INSERT INTO public.doctor VALUES ('0d72cffc-b597-4e62-ad6e-ff7c3a800960', 'Kolen', 'Viv', '12:10:00', '19:00:00');
INSERT INTO public.doctor VALUES ('05148c18-f899-42ef-b6a3-b25a44ba54a0', 'Bob', 'Marley', '08:00:00', '18:30:00');
INSERT INTO public.doctor VALUES ('433f89e5-140c-470d-af8f-9b3b6a109144', 'Sashka', 'Malina', '00:10:00', '19:20:00');


--
-- TOC entry 2827 (class 0 OID 16843)
-- Dependencies: 198
-- Data for Name: patient; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.patient VALUES ('2613f262-062c-4c0b-b149-c3f6b72f58c3', 'Tina', 'Karol', '1999-04-13', '+380992248006');
INSERT INTO public.patient VALUES ('1203cc33-2c50-42b4-a87e-55ce29b670ac', 'Ssdewd', 'Wwdwed', '2018-11-15', '+380992248006');
INSERT INTO public.patient VALUES ('4b4e7713-59f7-4e8d-873f-a9624f6e8d4b', 'Jena', 'Mitchel', '2018-11-06', '+380502248008');
INSERT INTO public.patient VALUES ('7e141671-6db4-4272-8253-d78eef358690', 'Amanda', 'Robins', '1999-12-12', '+380502248009');
INSERT INTO public.patient VALUES ('97adb6da-eece-4560-8c13-ee6f20ac1191', 'Mike', 'Non', '1925-06-16', '+380502248009');
INSERT INTO public.patient VALUES ('149f4c49-9347-40f4-a755-dcef58f515c9', 'Dina', 'Toms', '1997-05-10', '+380968748006');
INSERT INTO public.patient VALUES ('d038777c-3afa-42a0-8ada-dd5a26d02905', 'David', 'Kolen', '1991-11-12', '+380992248006');
INSERT INTO public.patient VALUES ('107c3ad7-f112-424e-b4c3-06ef1432d469', 'Tina', 'Hot', '1999-12-12', '+380992248006');


--
-- TOC entry 2828 (class 0 OID 16848)
-- Dependencies: 199
-- Data for Name: records; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.records VALUES ('15c3e988-b443-4d53-9503-7d065bfa41b6', 'fc672633-056a-4922-a453-bbd757f3015c', '2613f262-062c-4c0b-b149-c3f6b72f58c3', '09:45:00', '10:20:00');
INSERT INTO public.records VALUES ('0f8b9b43-8b36-4272-9af3-9c55bfb7d753', 'fc672633-056a-4922-a453-bbd757f3015c', '149f4c49-9347-40f4-a755-dcef58f515c9', '10:35:00', '11:24:00');
INSERT INTO public.records VALUES ('e4cf7c8f-31a8-4c6b-b4ec-7af2628be995', '0d72cffc-b597-4e62-ad6e-ff7c3a800960', '2613f262-062c-4c0b-b149-c3f6b72f58c3', '15:00:00', '15:30:00');
INSERT INTO public.records VALUES ('d3ab5006-a212-4515-ba06-4107940aef35', '433f89e5-140c-470d-af8f-9b3b6a109144', '2613f262-062c-4c0b-b149-c3f6b72f58c3', '15:35:00', '16:40:00');
INSERT INTO public.records VALUES ('3371726a-35d8-4b02-8d9c-687d57bcde5e', '433f89e5-140c-470d-af8f-9b3b6a109144', '2613f262-062c-4c0b-b149-c3f6b72f58c3', '16:40:00', '17:25:00');
INSERT INTO public.records VALUES ('a7251c97-18e6-4c90-98d0-cf0f7083c340', '05148c18-f899-42ef-b6a3-b25a44ba54a0', '2613f262-062c-4c0b-b149-c3f6b72f58c3', '14:35:00', '14:55:00');
INSERT INTO public.records VALUES ('6eb721fa-804c-4f55-b382-03e960f0f31d', '0d72cffc-b597-4e62-ad6e-ff7c3a800960', '97adb6da-eece-4560-8c13-ee6f20ac1191', '17:00:00', '17:45:00');
INSERT INTO public.records VALUES ('05254e2c-f484-4bb3-9b46-0c8577c2d5cd', '433f89e5-140c-470d-af8f-9b3b6a109144', 'd038777c-3afa-42a0-8ada-dd5a26d02905', '18:20:00', '19:00:00');
INSERT INTO public.records VALUES ('2ed2cfe1-dce7-4ae0-b55d-3dfeea84113c', '433f89e5-140c-470d-af8f-9b3b6a109144', 'd038777c-3afa-42a0-8ada-dd5a26d02905', '19:05:00', '19:20:00');


--
-- TOC entry 2829 (class 0 OID 16864)
-- Dependencies: 200
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.users VALUES ('107c3ad7-f112-424e-b4c3-06ef1432d469', 'tina', '$2a$15$nS7CvwVBmvxdcxFxxdi.P.JL1zQNai.6XbWzpn6sUzCC8Ru4btg5W', 'PATIENT', true);
INSERT INTO public.users VALUES ('0886ba2b-4163-4a77-aba6-75962581ffbd', 'admin', '$2a$15$qkHzUra2l4v6o0r6ir4kCuCx122xhqCSqls58Owu8VddOseWxz6Fa', 'ADMIN', true);
INSERT INTO public.users VALUES ('0d72cffc-b597-4e62-ad6e-ff7c3a800960', 'viv12', '$2a$15$esQlefkXFtNKWzi/HbeSYurNOZUliEkptZ59.Tn.om4lr1suH45Yq', 'DOCTOR', true);
INSERT INTO public.users VALUES ('2613f262-062c-4c0b-b149-c3f6b72f58c3', 'karol', '$2a$15$ewFStSJi7NFG.n.IAExY9uEC.sO90OFewgGtaLv0WWMf0MR1Bcoz6', 'PATIENT', true);
INSERT INTO public.users VALUES ('149f4c49-9347-40f4-a755-dcef58f515c9', 'dina', '$2a$15$XHK6sjewfNykd.HJ89UBQeky4.mHRBi3Gk605Z.zjoRTJWCRiZwYG', 'PATIENT', true);
INSERT INTO public.users VALUES ('fc672633-056a-4922-a453-bbd757f3015c', 'doctor', '$2a$15$XMTJK.SkjHIfpwOZIt5SOOW7557fObq.7xcwY6XiZ1XWjBmouOUVG', 'DOCTOR', true);
INSERT INTO public.users VALUES ('05148c18-f899-42ef-b6a3-b25a44ba54a0', 'bobM', '$2a$15$xi9A6TsZZIKMesx5K6Tz5OhPQ8Y32hw0Q1rgrfNw9ylmIu4lzp6pa', 'DOCTOR', true);
INSERT INTO public.users VALUES ('4b4e7713-59f7-4e8d-873f-a9624f6e8d4b', 'mitch', '$2a$15$ovzIExUSLpdVM.TLjjVh/uW4um.eC2Y7ApSUZcXdOFso1arajNkZK', 'PATIENT', true);
INSERT INTO public.users VALUES ('7e141671-6db4-4272-8253-d78eef358690', 'amaRob', '$2a$15$sVpD/vyii3qgY0jVWJcLP.FS.Z13K7TJreIGIFf41WWtFIuaWuUrS', 'PATIENT', true);
INSERT INTO public.users VALUES ('1203cc33-2c50-42b4-a87e-55ce29b670ac', 'qqq', '$2a$15$Pa2p9wXm9XU2h05ZM7lDDOi51ljioCfwlqaMFCJEqm6x1BoZfF4cy', 'PATIENT', false);
INSERT INTO public.users VALUES ('97adb6da-eece-4560-8c13-ee6f20ac1191', 'mike', '$2a$15$BiB6fXOLNoGg/zXXO8YrK./mH1wbgG1AYR7HigzajVT.k61iI.TUi', 'PATIENT', true);
INSERT INTO public.users VALUES ('d038777c-3afa-42a0-8ada-dd5a26d02905', 'david', '$2a$15$o9e/KE/3kNO2z5HF/rE4C.QvZRiQV1WerHgXu/idxJabaD0Ma8QFq', 'PATIENT', true);
INSERT INTO public.users VALUES ('433f89e5-140c-470d-af8f-9b3b6a109144', 'Doctor', '$2a$15$R9Y5fzXLW14cNUqjFLfXWOshzZuvbm9iOAV0qyJKEbMARR6zgNevm', 'DOCTOR', true);


-- Completed on 2018-11-21 19:55:11

--
-- PostgreSQL database dump complete
--

